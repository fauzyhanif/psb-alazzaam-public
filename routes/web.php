<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/generate-x', 'FormulirController@generateNoPendaftaran');

# home page
Route::get('/', function () {
    return view('front.home.index');
});

# Dashboard
Route::get('/dashboard', 'DashboardController@index')->middleware('admin');

# Helper
Route::get('/helpers/cari-kota/{id}', 'HelpersController@cariKota');
Route::get('/helpers/cari-kecamatan/{id}', 'HelpersController@cariKecamatan');
Route::get('/helpers/cari-desa/{id}', 'HelpersController@cariDesa');

# formulir page
Route::get('/formulir/{idJenjang}/{jnsKelamin}', 'FormulirController@index');
Route::post('/formulir/daftar', 'FormulirController@daftar');
Route::get('/pendaftaran-berhasil/{id}', 'FormulirController@daftarBerhasil');

# Download Berkas
Route::get('/download-berkas/bukti-pendaftaran/{noPendaftaran}', 'DownloadBerkasController@buktiPendaftaran');
Route::get('/download-berkas/lanjutan-pendaftaran/{noPendaftaran}', 'DownloadBerkasController@lanjutanPendaftaran');
Route::get('/download-berkas/kesanggupan/{noPendaftaran}', 'DownloadBerkasController@kesanggupan');
Route::get('/download-berkas/undangan/{noPendaftaran}', 'DownloadBerkasController@undangan');

# syarat page
Route::get('/syarat', function () {
    return view('front.syarat.index');
});

# prosedur page
Route::get('/prosedur', function () {
    return view('front.prosedur.index');
});

# Informasi Pendaftaran
Route::get('/informasi-psb', 'InformasiPsbController@index');
Route::get('/informasi-psb/download-brosur', 'InformasiPsbController@downloadBrosur');

# pengumuman page
Route::get('/pengumuman', function () {
    return view('pengumuman.index');
});

Route::get('/detail-pengumuman', function () {
    return view('pengumuman.detail');
});

# Auth page
Route::get('/auth', 'AuthController@index');
Route::post('/doAuth', 'AuthController@doAuth');
Route::get('/auth/pilih-usergroup', 'AuthController@pilihUsergroup');
Route::get('/set-usergroup/{id}', 'AuthController@setUsergroup');
Route::get('/logout', 'AuthController@logout');
Route::get('/access-denied', 'AuthController@accessDenied');

# Pilih Sesi
Route::get('/pilih-sesi', 'SessionController@pilihSesiTahunAkd');
Route::get('/set-sesi-tahun-akd/{thnAkd}/{gelombang}', 'SessionController@setSesiTahunAkd');

# User page
Route::get('/user', 'UserController@index')->middleware('admin');
Route::get('/user-json', 'UserController@json')->middleware('admin');
Route::get('/user/form-add', 'UserController@formAdd')->middleware('admin');
Route::post('/user/add-new', 'UserController@addNew')->middleware('admin');
Route::get('/user/form-edit/{id}', 'UserController@formEdit')->middleware('admin');
Route::post('/user/edit/{id}', 'UserController@edit')->middleware('admin');
Route::get('/user/delete/{id}', 'UserController@delete')->middleware('admin');
Route::get('/user/reset-password/{id}', 'UserController@resetPassword')->middleware('admin');

# Usergroup page
Route::get('/usergroup', 'UsergroupController@index')->middleware('admin');
Route::get('/usergroup/form-add', 'UsergroupController@formAdd')->middleware('admin');
Route::post('/usergroup/add-new', 'UsergroupController@addNew')->middleware('admin');
Route::get('/usergroup/form-edit/{id}', 'UsergroupController@formEdit')->middleware('admin');
Route::post('/usergroup/edit/{id}', 'UsergroupController@edit')->middleware('admin');
Route::get('/usergroup/delete/{id}', 'UsergroupController@delete')->middleware('admin');

# Jenjang Pendidikan page
Route::get('/jenjang', 'JenjangController@index')->middleware('admin');
Route::get('/jenjang/form-add', 'JenjangController@formAdd')->middleware('admin');
Route::post('/jenjang/add-new', 'JenjangController@addNew')->middleware('admin');
Route::get('/jenjang/form-edit/{id}', 'JenjangController@formEdit')->middleware('admin');
Route::post('/jenjang/edit/{id}', 'JenjangController@edit')->middleware('admin');
Route::get('/jenjang/delete/{id}', 'JenjangController@delete')->middleware('admin');

# Sesi page
Route::get('/sesi', 'SesiController@index')->middleware('admin');
Route::get('/sesi/form-add', 'SesiController@formAdd')->middleware('admin');
Route::post('/sesi/add-new', 'SesiController@addNew')->middleware('admin');
Route::get('/sesi/form-edit/{id}', 'SesiController@formEdit')->middleware('admin');
Route::post('/sesi/edit/{id}', 'SesiController@edit')->middleware('admin');
Route::get('/sesi/delete/{id}', 'SesiController@delete')->middleware('admin');

# Setting page
Route::get('/setting', 'SettingController@index')->middleware('admin');
Route::get('/setting/form-add', 'SettingController@formAdd')->middleware('admin');
Route::post('/setting/add-new', 'SettingController@addNew')->middleware('admin');
Route::get('/setting/form-edit/{id}', 'SettingController@formEdit')->middleware('admin');
Route::post('/setting/edit/{id}', 'SettingController@edit')->middleware('admin');
Route::get('/setting/delete/{id}', 'SettingController@delete')->middleware('admin');

# Admin Data Calon Santri page
Route::get('/data-calon-santri/{noPendaftaran?}', 'AdminCalonSantriController@index')->middleware('admin', 'sesi-tahun-akd');
Route::get('/data-calon-santri-export', 'AdminCalonSantriController@export')->middleware('admin');
Route::get('/json-data-calon-santri/{jenjang?}', 'AdminCalonSantriController@json')->middleware('admin');
Route::get('/admin/profil-calon-santri/{noPendaftaran}', 'AdminCalonSantriController@profilCalonSantri')->middleware('admin');
Route::get('/admin/validasi-dokumen/{noPendaftaran}', 'AdminCalonSantriController@validasiDokumen')->middleware('admin');
Route::post('/admin/do-validasi-dokumen/{noPendaftaran}', 'AdminCalonSantriController@doValidasiDokumen')->middleware('admin');
Route::get('/admin/validasi-pembayaran/{noPendaftaran}', 'AdminCalonSantriController@validasiPembayaran')->middleware('admin');
Route::post('/admin/do-validasi-pembayaran/{noPendaftaran}', 'AdminCalonSantriController@doValidasiPembayaran')->middleware('admin');
Route::get('/admin/set-undangan/{noPendaftaran}', 'AdminCalonSantriController@setUndangan')->middleware('admin');
Route::post('/admin/do-set-undangan/{noPendaftaran}', 'AdminCalonSantriController@doSetUndangan')->middleware('admin');

# Calon santri page
Route::get('/calon-santri/profil', 'CalonSantriController@profil')->middleware('calon-santri');
Route::get('/calon-santri/upload-dokumen', 'CalonSantriController@uploadDokumen')->middleware('calon-santri');
Route::post('/calon-santri/do-upload-dokumen', 'CalonSantriController@doUploadDokumen')->middleware('calon-santri');
Route::get('/calon-santri/upload-bukti-pembayaran', 'CalonSantriController@uploadBuktiPembayaran')->middleware('calon-santri');
Route::post('/calon-santri/do-upload-bukti-pembayaran', 'CalonSantriController@doUploadBuktiPembayaran')->middleware('calon-santri');
Route::get('/calon-santri/edit-profil', 'CalonSantriController@editProfil')->middleware('calon-santri');
Route::post('/calon-santri/do-edit-profil/{noPendaftaran}', 'CalonSantriController@doEditProfil')->middleware('calon-santri');
