<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbAkdCsntrSekolahAsal extends Model
{
    protected $table = "psb_akd_csntr_sekolah_asal";
    protected $primaryKey = "no_pendaftaran";
    protected $fiilable = [
        "id",
        "no_pendaftaran",
        "nama",
        "nspn",
        "thn_lulus",
        "alamat",
        "id_desa", 
        "id_kecamatan",
        "id_kota",
        "id_provinsi",
        "nilai_bhs_indonesia",
        "nilai_bhs_inggris",
        "nilai_matematika",
        "nilai_ipa",
        "nilai_bhs_indonesia_2",
        "nilai_bhs_inggris_2",
        "nilai_matematika_2",
        "nilai_ipa_2"
    ];
}
