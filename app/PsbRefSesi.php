<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PsbRefSesi extends Model
{
    use SoftDeletes;
    public $table = 'psb_ref_sesi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama',
        'thn_akd',
        'gelombang',
        'tgl_mulai',
        'tgl_selesai',
        'tes_tgl',
        'tes_jam',
        'tes_tempat',
        'tes_materi',
        'biaya',
        'is_aktif',
        'deleted_at'
    ];
}
