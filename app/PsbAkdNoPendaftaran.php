<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbAkdNoPendaftaran extends Model
{
    public $table = 'psb_akd_no_pendaftaran';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_jenjang',
        'thn_akd',
        'jns_kelamin',
        'no_urut'
    ];
}
