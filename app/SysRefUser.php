<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SysRefUser extends Model
{
    protected $table = 'sys_ref_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'nama',
        'username',
        'password',
        'id_usergroup',
        'id_jenjang',
        'is_aktif'
    ];
}
