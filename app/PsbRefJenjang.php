<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbRefJenjang extends Model
{
    public $table = 'psb_ref_jenjang';
    protected $primaryKey = 'id_jenjang';
    protected $fillable = [
        'id_jenjang',
        'nama',
        'is_aktif'
    ];
}
