<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRefPenghasilan extends Model
{
    protected $table = "sys_ref_penghasilan";
    protected $primaryKey = "id_penghasilan";
    protected $fiilable = [
        "id_penghasilan",
        "nm_penghasilan",
        "batas_bawah",
        "batas_atas",
    ];
}
