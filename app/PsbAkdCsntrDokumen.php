<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbAkdCsntrDokumen extends Model
{
    protected $table = "psb_akd_csntr_dokumen";
    protected $primaryKey = "id";
    protected $fiilable = [
        "id",
        "no_pendaftaran",
        "id_dokumen",
        "file"
    ];
}
