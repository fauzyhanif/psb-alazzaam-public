<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SysRefUsergroup extends Model
{
    protected $table = 'sys_ref_usergroup';
    protected $primaryKey = 'id_usergroup';
    protected $fillable = [
        'id_usergroup',
        'nama',
        'is_aktif'
    ];
}
