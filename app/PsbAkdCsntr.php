<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbAkdCsntr extends Model
{
    protected $table = "psb_akd_csntr";
    protected $primaryKey = "no_pendaftaran";
    public $incrementing = false;
    protected $fiilable = [
        "id",
        "no_pendaftaran",
        "password",
        "id_jenjang",
        "thn_akd",
        "gelombang",
        "nama",
        "tmp_lahir",
        "tgl_lahir",
        "jml_sdr",
        "anak_ke",
        "gol_darah",
        "jns_kelamin",
        "alamat",
        "rt",
        "rw",
        "id_desa",
        "id_kecamatan",
        "id_kota",
        "id_provinsi",
        "kode_pos",
        "jml_hafalan",
        "nisn",
        "nik",
        "kk",
        "pj_biaya",
        "file_bukti_pembayaran",
        "konfirmasi_bukti_pembayaran",
        "biaya",
        "tgl_daftar",
    ];
}
