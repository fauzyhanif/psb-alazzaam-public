<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRefSetting extends Model
{
    protected $table = "sys_ref_setting";
    protected $primaryKey = "id_unit";
    protected $fiilable = [
        "id_unit",
        "nama",
        "logo",
        "alamat",
        "no_hp_1",
        "no_hp_2",
        "email",
        "tahun_ke",
    ];
}
