<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbRefSetting extends Model
{
    public $table = 'psb_ref_setting';
    protected $fillable = [
        'parameter',
        'description',
        'value'
    ];
}
