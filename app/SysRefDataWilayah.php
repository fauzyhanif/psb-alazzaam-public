<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRefDataWilayah extends Model
{
    protected $table = "sys_ref_data_wilayah";
    protected $primaryKey = "id";
    protected $fiilable = [
        "id",
        "id_wil",
        "nm_wil",
        "id_induk_wilayah",
        "id_level_wil",
        "status_error",
    ];
}
