<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbAkdCsntrAyah extends Model
{
    protected $table = "psb_akd_csntr_ayah";
    protected $primaryKey = "no_pendaftaran";
    protected $fiilable = [
        "id",
        "no_pendaftaran",
        "nama",
        "tmp_lahir",
        "tgl_lahir",
        "id_pendidikan",
        "id_pekerjaan",
        "id_penghasilan",
        "no_hp",
        "status_hidup",
        "nik"
    ];
}
