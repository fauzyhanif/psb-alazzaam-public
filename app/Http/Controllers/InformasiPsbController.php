<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use Response;

class InformasiPsbController extends Controller
{
    public function index()
    {
        return view('front.informasi_psb.index');
    }

    public function downloadBrosur()
    {
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/img/Brosur_PSB_2024.pdf";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return Response::download($file, 'Brosur-PSB-PPTQ-AlAzzaam.pdf', $headers);
    }
}
