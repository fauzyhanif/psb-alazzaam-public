<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PsbRefSetting;
use App\PsbRefJenjang;

class SettingController extends Controller
{
    public function index()
    {
        $datas = PsbRefSetting::orderBy("parameter", "desc")->get();
        return view('admin.referensi.setting.index', compact('datas'));
    }
    public function formAdd(Request $request)
    {
        return view('admin.referensi.setting.add');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'parameter'=> ['required', 'unique:psb_ref_setting,value'],
            'description' => 'required',
            'value' => 'required'
        ]);

        $new = new PsbRefSetting();
        $new->parameter = $request->parameter;
        $new->description = $request->description;
        $new->value = $request->value;
        $new->save();

        return redirect()->back()->with('success', 'setting berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data = PsbRefSetting::findOrFail($id);
        return view('admin.referensi.setting.edit', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'parameter'=> ['required'],
            'description' => 'required',
            'value' => 'required'
        ]);

        $data = PsbRefSetting::findOrFail($id);
        $data->description = $request->description;
        $data->value = $request->value;
        $data->update();

        return redirect()->back()->with('success', 'setting berhasil diubah');
    }

    public function delete($id)
    {
        $data = PsbRefSetting::find($id);
        $data->delete();
        
        return redirect('/setting')->with('success', 'setting berhasil dihapus');
    }
}
