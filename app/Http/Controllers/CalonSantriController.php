<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

use App\PsbAkdCsntr;
use App\PsbAkdCsntrDokumen;
use App\PsbAkdCsntrSekolahAsal;
use App\PsbAkdCsntrAyah;
use App\PsbAkdCsntrIbu;
use App\PsbRefDokumen;

class CalonSantriController extends Controller
{
    public function profil()
    {
        $noPendaftaran = Session::get('username');
        
        $data = DB::table('psb_akd_csntr as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->leftJoin('psb_ref_jenjang as e', 'a.id_jenjang', '=', 'e.id_jenjang')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi', 'e.nama as jenjang')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtSekolah = DB::table('psb_akd_csntr_sekolah_asal as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtAyah = DB::table('psb_akd_csntr_ayah as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtIbu = DB::table('psb_akd_csntr_ibu as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        return view('calonSantri.profil', compact('data', 'dtSekolah', 'dtAyah', 'dtIbu'));
    }

    public function uploadDokumen()
    {
        $noPendaftaran = Session::get('username');
        $dtForUpload = DB::table('psb_akd_csntr_dokumen as a')
            ->leftJoin('psb_ref_dokumen as b', 'a.id_dokumen', '=', 'b.id')
            ->select('a.*', 'b.nama as nm_dokumen')
            ->where('a.no_pendaftaran', '=', "$noPendaftaran")
            ->where('file', '!=', '')
            ->get();

        $dtRefDokumen = PsbRefDokumen::orderBy('orders', 'ASC')->get();

        $dtGeneral = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $arrDokumenUploaded = [];
        foreach ($dtForUpload as $key => $value) {
            $arrDokumenUploaded[$value->id_dokumen] = $value->file;
        }

        $status = $dtGeneral->status_upload_dokumen;
        $catatan = $dtGeneral->catatan_upload_dokumen;

        return view('calonSantri.uploadDokumen', compact('dtForUpload', 'status', 'catatan', 'dtGeneral', 'dtRefDokumen', 'arrDokumenUploaded'));
    }

    public function doUploadDokumen(Request $request)
    {
        $file = $request->file('filename');
        $idDokumen = $request->get('id_dokumen');
        $noPendaftaran = Session::get('username');

        // check dokumen stored on dokumen csntr
        $check_exists = DB::table('psb_akd_csntr_dokumen')
            ->where('no_pendaftaran', "$noPendaftaran")
            ->where('id_dokumen', "$idDokumen")
            ->first();

        if ($check_exists !== null) {
            if ($check_exists->file != '') {
                // remove old file
                File::delete('dokumen-pendaftar/'.  $check_exists->file);
            }
        } else {
            $new = new PsbAkdCsntrDokumen();
            $new->no_pendaftaran = $noPendaftaran;
            $new->id_dokumen = $idDokumen;
            $new->file = '';
            $new->save();
        }

        // rename filename
        $name = $noPendaftaran . '-' . $idDokumen . '.' . $file->getClientOriginalExtension();

        // upload file
        $file->move('dokumen-pendaftar', $name);

        // record db
        $data = DB::table('psb_akd_csntr_dokumen')
            ->where('no_pendaftaran', "$noPendaftaran")
            ->where('id_dokumen', "$idDokumen")
            ->update(["file" => $name]);

        return redirect('/calon-santri/upload-dokumen')->with('success', 'File berhasil diupload');
    }

    public function uploadBuktiPembayaran()
    {
        $noPendaftaran = Session::get('username');
        $data = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        return view('calonSantri.uploadBuktiPembayaran', compact('data'));
    }

    public function doUploadBuktiPembayaran(Request $request)
    {
        $file = $request->file('filename');
        $noPendaftaran = Session::get('username');

        // check if file has uploaded
        $check = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        if ($check->file_bukti_pembayaran != '') {
            // remove old file
            File::delete('dokumen-pendaftar'.  $check->file_bukti_pembayaran);
        }

        // rename filename
        $name = $noPendaftaran . '-bukti-transfer.' . $file->getClientOriginalExtension();

        // upload file
        $file->move('dokumen-pendaftar', $name);

        // record db
        $data = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->update(["file_bukti_pembayaran" => $name]);

        return redirect('/calon-santri/upload-bukti-pembayaran')->with('success', 'File Bukti Pembayaran berhasil diupload');
    }

    public function editProfil()
    {
        $noPendaftaran = Session::get('username');

        $dtGeneral = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtSekolah = DB::table('psb_akd_csntr_sekolah_asal')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtAyah = DB::table('psb_akd_csntr_ayah')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtIbu = DB::table('psb_akd_csntr_ibu')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        return view('calonSantri.editProfil', compact('dtGeneral', 'dtSekolah', 'dtAyah', 'dtIbu'));
    }

    public function doEditProfil(Request $request, $noPendaftaran)
    {
        $data = $request->all();

        // call function save data
        $this->saveDataGeneral($noPendaftaran, $data);
        $this->saveDataSekolahAsal($noPendaftaran, $data);
        $this->saveDataAyah($noPendaftaran, $data);
        $this->saveDataIbu($noPendaftaran, $data);

        return json_encode("okee");
    }

    public function saveDataGeneral($noPendaftaran, $data)
    {
        $oldData                    = PsbAkdCsntr::where('no_pendaftaran', '=', $noPendaftaran)->firstOrFail();
        $oldData->id_jenjang        = $data['id_jenjang'];
        $oldData->nama              = $data['nama'];
        $oldData->tmp_lahir         = $data['tmp_lahir'];
        $oldData->tgl_lahir         = $data['tgl_lahir'];
        $oldData->jml_sdr           = $data['jml_sdr'];
        $oldData->anak_ke           = $data['anak_ke'];
        $oldData->jns_kelamin       = $data['jns_kelamin'];
        $oldData->gol_darah         = $data['gol_darah'];
        $oldData->alamat            = $data['alamat'];
        $oldData->rt                = $data['rt'];
        $oldData->rw                = $data['rw'];
        $oldData->id_desa           = $data['id_desa'];
        $oldData->id_kecamatan      = $data['id_kecamatan'];
        $oldData->id_kota           = $data['id_kota'];
        $oldData->id_provinsi       = $data['id_provinsi'];
        $oldData->kode_pos          = $data['kode_pos'];
        $oldData->nisn              = $data['nisn'];
        $oldData->nik               = $data['nik'];
        $oldData->kk                = $data['kk'];
        $oldData->pj_biaya          = $data['pj_biaya'];
        $oldData->status_anak       = $data['status_anak'];
        $oldData->membaca_alquran   = $data['membaca_alquran'];
        $oldData->siap_diuji_hafalan= $data['siap_diuji_hafalan'];
        $oldData->update();
    }

    public function saveDataSekolahAsal($noPendaftaran, $data)
    {
        $oldData                        = PsbAkdCsntrSekolahAsal::where('no_pendaftaran', '=', $noPendaftaran)->firstOrFail();
        $oldData->nama                  = $data['sekolah_nama'];
        $oldData->nspn                  = $data['sekolah_nspn'];
        $oldData->id_kecamatan          = $data['sekolah_id_kecamatan'];
        $oldData->id_kota               = $data['sekolah_id_kota'];
        $oldData->id_provinsi           = $data['sekolah_id_provinsi'];
        $oldData->nilai_bhs_indonesia   = (isset($data['nilai_bhs_indonesia'])) ? str_replace(',', '.', $data['nilai_bhs_indonesia']) : 0;
        $oldData->nilai_bhs_inggris     = (isset($data['nilai_bhs_inggris'])) ? str_replace(',', '.', $data['nilai_bhs_inggris']) : 0;
        $oldData->nilai_matematika      = (isset($data['nilai_matematika'])) ? str_replace(',', '.', $data['nilai_matematika']) : 0;
        $oldData->nilai_ipa             = (isset($data['nilai_ipa'])) ? str_replace(',', '.', $data['nilai_ipa']) : 0;
        $oldData->nilai_bhs_indonesia_2 = (isset($data['nilai_bhs_indonesia_2'])) ? str_replace(',', '.', $data['nilai_bhs_indonesia_2']) : 0;
        $oldData->nilai_bhs_inggris_2   = (isset($data['nilai_bhs_inggris_2'])) ? str_replace(',', '.', $data['nilai_bhs_inggris_2']) : 0;
        $oldData->nilai_matematika_2    = (isset($data['nilai_matematika_2'])) ? str_replace(',', '.', $data['nilai_matematika_2']) : 0;
        $oldData->nilai_ipa_2           = (isset($data['nilai_ipa_2'])) ? str_replace(',', '.', $data['nilai_ipa_2']) : 0;
        $oldData->update();
    }

    public function saveDataAyah($noPendaftaran, $data)
    {
        $oldData = PsbAkdCsntrAyah::where('no_pendaftaran', '=', $noPendaftaran)->firstOrFail();
        $oldData->nama              = $data['ayah_nama'];
        $oldData->tmp_lahir         = $data['ayah_tmp_lahir'];
        $oldData->tgl_lahir         = $data['ayah_tgl_lahir'];
        $oldData->id_pendidikan     = $data['ayah_id_pendidikan'];
        $oldData->id_pekerjaan      = $data['ayah_id_pekerjaan'];
        $oldData->no_hp             = $data['ayah_no_hp'];
        $oldData->status_hidup      = $data['ayah_status_hidup'];
        $oldData->nik               = $data['ayah_nik'];
        $oldData->domisili_sesuai_kk= $data['ayah_domisili_sesuai_kk'];
        $oldData->update();
    }

    public function saveDataIbu($noPendaftaran, $data)
    {
        $oldData = PsbAkdCsntrIbu::where('no_pendaftaran', '=', $noPendaftaran)->firstOrFail();;
        $oldData->nama              = $data['ibu_nama'];
        $oldData->tmp_lahir         = $data['ibu_tmp_lahir'];
        $oldData->tgl_lahir         = $data['ibu_tgl_lahir'];
        $oldData->id_pendidikan     = $data['ibu_id_pendidikan'];
        $oldData->id_pekerjaan      = $data['ibu_id_pekerjaan'];
        $oldData->no_hp             = $data['ibu_no_hp'];
        $oldData->status_hidup      = $data['ibu_status_hidup'];
        $oldData->nik               = $data['ibu_nik'];
        $oldData->domisili_sesuai_kk= $data['ibu_domisili_sesuai_kk'];
        $oldData->update();
    }
}
