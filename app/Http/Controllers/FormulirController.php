<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use App\PsbAkdNoPendaftaran;
use App\PsbAkdCsntr;
use App\PsbAkdCsntrSekolahAsal;
use App\PsbAkdCsntrAyah;
use App\PsbAkdCsntrIbu;
use App\SysRefUser;
use App\PsbRefDokumen;
use App\PsbAkdCsntrDokumen;

class FormulirController extends Controller
{
    public function index($idJenjang, $jnsKelamin)
    {
        $datas['idJenjang'] = $idJenjang;
        $datas['jnsKelamin'] = $jnsKelamin;

        $this_day = date('Y-m-d');

        $data = DB::table('psb_ref_sesi')
            ->where('tgl_mulai', '<=', $this_day)
            ->where('tgl_selesai', '>=', $this_day)
            ->where('is_aktif', '=', 'Y')
            ->first();

        if ($data !== null) {
            return view('front.formulir.formulir', compact('datas'));
        } else {
            return view('front.formulir.close');
        }
    }

    public function daftar(Request $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->all();

            // get generate no pendaftaran
            $data['no_pendaftaran'] = $this->generateNoPendaftaran($data);

            // generate token
            $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUvWXYZ';
            $data['token'] =  substr(str_shuffle($permitted_chars), 0, 20);

            // generate random password
            $data['password'] = $data['jns_kelamin'] . '' . substr($data['no_pendaftaran'], -3);

            // generate biaya pendaftaran
            $biayaAsli = ($data['id_jenjang'] == '2') ? '200000' : $this->getBiaya($data);
            $digitAwal = substr($biayaAsli,0,3);
            $noUrutPendaftaran = substr($data['no_pendaftaran'], -3);
            $data['biaya'] = $digitAwal . $noUrutPendaftaran;

            if (!isset($data['jalur'])) {
                $data['jalur'] = 'REGULER';
            }

            // call save data functions
            $this->saveDataGeneral($data);
            $this->saveDataSekolahAsal($data);
            $this->saveDataAyah($data);
            $this->saveDataIbu($data);
            $this->saveDataUser($data);
            $this->saveDataDokumen($data);
            
            DB::commit();
            return json_encode($data['token']);
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function getBiaya($data)
    {
        $thnAkd = $data['thn_akd'] . '/' . ($data['thn_akd'] + 1);
        $dtBiaya = DB::table('psb_ref_sesi')
            ->where('thn_akd', '=', $thnAkd)
            ->where('gelombang', '=', $data['gelombang'])
            ->first();

        return $dtBiaya->biaya;
    }

    public function generateNoPendaftaran($data)
    {
        // search last no_urut
        $dataNoPendaftaran = PsbAkdNoPendaftaran::where('thn_akd', $data['thn_akd'])
            ->where('id_jenjang', $data['id_jenjang'])
            ->where('jns_kelamin', $data['jns_kelamin'])
            ->first();
            
        if (!$dataNoPendaftaran) {
            $noUrut = '001';
            $new = new PsbAkdNoPendaftaran();
            $new->id_jenjang = $data['id_jenjang'];
            $new->thn_akd = $data['thn_akd'];
            $new->jns_kelamin = $data['jns_kelamin'];
            $new->no_urut = sprintf('%03d', $noUrut) + 1;
            $new->save();
        } else {
            $noUrut = sprintf('%03d', $dataNoPendaftaran->no_urut);
            $dataNoPendaftaran->no_urut = sprintf('%03d', $dataNoPendaftaran->no_urut) + 1;
            $dataNoPendaftaran->save();
        }

        $jenjang = '';
        if ($data['id_jenjang'] == '1') {
            $jenjang = 'SMP';
        } elseif ($data['id_jenjang'] == '2') {
            $jenjang = 'IM';
        } elseif ($data['id_jenjang'] == '3') {
            $jenjang = 'MA';
        }
        
        return $data['jns_kelamin'] . '-' . $jenjang . '-' . $data['thn_akd'] . '-' . $noUrut;
    }
    public function saveDataGeneral($data)
    {
        $data['tgl_lahir'] = date_create($data['thn_lahir'] . '-' . $data['bln_lahir'] . '-' . $data['tgl_lahir']);
        $data['nama'] = str_replace("'", '"', $data['nama']);

        $newData = new PsbAkdCsntr();
        $newData->id_jenjang        = $data['id_jenjang'];
        $newData->jalur             = $data['jalur'];
        $newData->thn_akd           = $data['thn_akd'];
        $newData->gelombang         = $data['gelombang'];
        $newData->no_pendaftaran    = $data['no_pendaftaran'];
        $newData->password          = $data['password'];
        $newData->nama              = $data['nama'];
        $newData->tmp_lahir         = $data['tmp_lahir'];
        $newData->tgl_lahir         = $data['tgl_lahir'];
        $newData->jml_sdr           = $data['jml_sdr'];
        $newData->anak_ke           = $data['anak_ke'];
        $newData->jns_kelamin       = $data['jns_kelamin'];
        $newData->gol_darah         = $data['gol_darah'];
        $newData->alamat            = $data['alamat'];
        $newData->rt                = $data['rt'];
        $newData->rw                = $data['rw'];
        $newData->id_desa           = $data['id_desa'];
        $newData->id_kecamatan      = $data['id_kecamatan'];
        $newData->id_kota           = $data['id_kota'];
        $newData->id_provinsi       = $data['id_provinsi'];
        $newData->kode_pos          = $data['kode_pos'];
        $newData->status_anak       = $data['status_anak'];
        $newData->membaca_alquran   = $data['membaca_alquran'];
        $newData->siap_diuji_hafalan= $data['siap_diuji_hafalan'];
        $newData->nisn              = $data['nisn'];
        $newData->nik               = $data['nik'];
        $newData->kk                = $data['kk'];
        $newData->pj_biaya          = $data['pj_biaya'];
        $newData->token             = $data['token'];
        $newData->biaya             = $data['biaya'];
        $newData->tgl_daftar        = date('Y-m-d');
        $newData->tes_tgl           = $data['tes_tgl'];
        $newData->tes_jam           = $data['tes_jam'];
        $newData->tes_tempat        = $data['tes_tempat'];
        $newData->tes_materi        = $data['tes_materi'];

        if ($data['id_jenjang'] != '2') {
            $newData->pip           = $data['pip'];
        }
        $newData->save();
    }

    public function saveDataSekolahAsal($data)
    {
        $newData = new PsbAkdCsntrSekolahAsal();
        $newData->no_pendaftaran        = $data['no_pendaftaran'];
        $newData->nama                  = $data['sekolah_nama'];
        $newData->nspn                  = $data['sekolah_nspn'];
        $newData->id_kecamatan          = $data['sekolah_id_kecamatan'];
        $newData->id_kota               = $data['sekolah_id_kota'];
        $newData->id_provinsi           = $data['sekolah_id_provinsi'];
        $newData->nilai_bhs_indonesia   = (isset($data['nilai_bhs_indonesia'])) ? str_replace(',', '.', $data['nilai_bhs_indonesia']) : 0;
        $newData->nilai_bhs_inggris     = (isset($data['nilai_bhs_inggris'])) ? str_replace(',', '.', $data['nilai_bhs_inggris']) : 0;
        $newData->nilai_matematika      = (isset($data['nilai_matematika'])) ? str_replace(',', '.', $data['nilai_matematika']) : 0;
        $newData->nilai_ipa             = (isset($data['nilai_ipa'])) ? str_replace(',', '.', $data['nilai_ipa']) : 0;
        $newData->nilai_bhs_indonesia_2 = (isset($data['nilai_bhs_indonesia_2'])) ? str_replace(',', '.', $data['nilai_bhs_indonesia_2']) : 0;
        $newData->nilai_bhs_inggris_2   = (isset($data['nilai_bhs_inggris_2'])) ? str_replace(',', '.', $data['nilai_bhs_inggris_2']) : 0;
        $newData->nilai_matematika_2    = (isset($data['nilai_matematika_2'])) ? str_replace(',', '.', $data['nilai_matematika_2']) : 0;
        $newData->nilai_ipa_2           = (isset($data['nilai_ipa_2'])) ? str_replace(',', '.', $data['nilai_ipa_2']) : 0;

        $saved = $newData->save();
    }

    public function saveDataAyah($data)
    {
        $data['ayah_thn_lahir'] = $data['ayah_thn_lahir'] == '' ? '0000' : $data['ayah_thn_lahir'];
        $data['ayah_bln_lahir'] = $data['ayah_bln_lahir'] == '' ? '00' : $data['ayah_bln_lahir'];
        $data['ayah_tgl_lahir'] = $data['ayah_tgl_lahir'] == '' ? '00' : $data['ayah_tgl_lahir'];
        $data['ayah_tgl_lahir'] = $data['ayah_thn_lahir'] . '-' . $data['ayah_bln_lahir'] . '-' . $data['ayah_tgl_lahir'];
        $data['ayah_nama']      = str_replace("'", '"', $data['ayah_nama']);

        $newData = new PsbAkdCsntrAyah();
        $newData->no_pendaftaran    = $data['no_pendaftaran'];
        $newData->nama              = $data['ayah_nama'];
        $newData->tmp_lahir         = $data['ayah_tmp_lahir'];
        $newData->tgl_lahir         = $data['ayah_tgl_lahir'];
        $newData->id_pendidikan     = $data['ayah_id_pendidikan'];
        $newData->id_pekerjaan      = $data['ayah_id_pekerjaan'];
        $newData->no_hp             = $data['ayah_no_hp'];
        $newData->status_hidup      = $data['ayah_status_hidup'];
        $newData->nik               = $data['ayah_nik'];
        $newData->domisili_sesuai_kk= $data['ayah_domisili_sesuai_kk'];
        $newData->save();
    }

    public function saveDataIbu($data)
    {
        $data['ibu_thn_lahir']  = $data['ibu_thn_lahir'] == '' ? '0000' : $data['ibu_thn_lahir'];
        $data['ibu_bln_lahir']  = $data['ibu_bln_lahir'] == '' ? '00' : $data['ibu_bln_lahir'];
        $data['ibu_tgl_lahir']  = $data['ibu_tgl_lahir'] == '' ? '00' : $data['ibu_tgl_lahir'];
        $data['ibu_tgl_lahir']  = $data['ibu_thn_lahir'] . '-' . $data['ibu_bln_lahir'] . '-' . $data['ibu_tgl_lahir'];
        $data['ibu_nama']       = str_replace("'", '"', $data['ibu_nama']);

        $newData = new PsbAkdCsntrIbu();
        $newData->no_pendaftaran    = $data['no_pendaftaran'];
        $newData->nama              = $data['ibu_nama'];
        $newData->tmp_lahir         = $data['ibu_tmp_lahir'];
        $newData->tgl_lahir         = $data['ibu_tgl_lahir'];
        $newData->id_pendidikan     = $data['ibu_id_pendidikan'];
        $newData->id_pekerjaan      = $data['ibu_id_pekerjaan'];
        $newData->no_hp             = $data['ibu_no_hp'];
        $newData->status_hidup      = $data['ibu_status_hidup'];
        $newData->nik               = $data['ibu_nik'];
        $newData->domisili_sesuai_kk= $data['ibu_domisili_sesuai_kk'];
        $newData->save();
    }

    public function saveDataUser($data)
    {
        $newData                = new SysRefUser();
        $newData->nama          = $data['nama'];
        $newData->username      = $data['no_pendaftaran'];
        $newData->password      = Hash::make($data['password']);
        $newData->id_usergroup  = ',4,';
        $newData->id_jenjang    = ',' . $data['id_jenjang'] . ',';
        $newData->is_aktif      = 'Y';
        $newData->save();
    }

    public function saveDataDokumen($data)
    {
        $dtDefDokumen = PsbRefDokumen::all();

        foreach ($dtDefDokumen as $key => $value) {
            $newData = new PsbAkdCsntrDokumen();
            $newData->no_pendaftaran = $data['no_pendaftaran'];
            $newData->id_dokumen = $value->id;
            $newData->file = '';
            $newData->save();
        }
    }

    public function daftarBerhasil($token)
    {
        $data = DB::table('psb_akd_csntr as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->leftJoin('psb_ref_jenjang as e', 'a.id_jenjang', '=', 'e.id_jenjang')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi', 'e.nama as jenjang')
            ->where('token', '=', $token)
            ->first();

        $dtUser = DB::table('sys_ref_user')->where('username', '=', $data->no_pendaftaran)->first();
        // set session
        Session::put('nama', $dtUser->nama);
        Session::put('username', $dtUser->username);
        Session::put('usergroup', $dtUser->id_usergroup);
        Session::put('jenjang', $dtUser->id_jenjang);
        Session::put('usergroup_aktif', str_replace(",", "", $dtUser->id_usergroup));


        $dtSekolah = DB::table('psb_akd_csntr_sekolah_asal as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi')
            ->where('no_pendaftaran', '=', $data->no_pendaftaran)
            ->first();

        $dtAyah = DB::table('psb_akd_csntr_ayah as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $data->no_pendaftaran)
            ->first();

        $dtIbu = DB::table('psb_akd_csntr_ibu as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $data->no_pendaftaran)
            ->first();

        return view('calonSantri.daftarBerhasil', compact('data', 'dtSekolah', 'dtAyah', 'dtIbu'));
    }
}
