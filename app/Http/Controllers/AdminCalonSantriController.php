<?php

namespace App\Http\Controllers;

use App\Helpers\HelperDashboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Session;
use DataTables;

use App\PsbAkdCsntr;
use App\PsbRefJenjang;
use App\ViewCalonSantriLengkap;
use App\PsbRefDokumen;

class AdminCalonSantriController extends Controller
{
    public function index($jenjang = '*')
    {
        $dtJenjang = PsbRefJenjang::where('is_aktif', '=', 'Y')->get();
        return view('admin.dataCalonSantri.index', compact('dtJenjang', 'jenjang'));
    }

    public function json($jenjang = '*')
    {
        $thnAkd = Session::get('thn_akd');
        $gelombang = Session::get('gelombang');

        if ($jenjang == '*') {
            $datas = DB::table('psb_akd_csntr as a')
                ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
                ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
                ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
                ->leftJoin('psb_ref_jenjang as e', 'a.id_jenjang', '=', 'e.id_jenjang')
                ->leftJoin('psb_akd_csntr_ayah as f', 'a.no_pendaftaran', '=', 'f.no_pendaftaran')
                ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi', 'e.nama as jenjang', 'f.no_hp')
                ->where('a.thn_akd', '=', $thnAkd)
                ->where('a.gelombang', '=', $gelombang);
        } else {
            $datas = DB::table('psb_akd_csntr as a')
                ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
                ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
                ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
                ->leftJoin('psb_ref_jenjang as e', 'a.id_jenjang', '=', 'e.id_jenjang')
                ->leftJoin('psb_akd_csntr_ayah as f', 'a.no_pendaftaran', '=', 'f.no_pendaftaran')
                ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi', 'e.nama as jenjang', 'f.no_hp')
                ->where('a.thn_akd', '=', $thnAkd)
                ->where('a.gelombang', '=', $gelombang)
                ->where('a.id_jenjang', '=', $jenjang);
        }

        return DataTables::of($datas)
            ->editColumn('jenjang' , function($datas){
                if ($datas->id_jenjang == 3) {
                    return $datas->jenjang . " ($datas->jalur)";
                } else {
                    return $datas->jenjang;
                }
            })
            ->editColumn('jns_kelamin' , function($datas){
                return $datas->jns_kelamin == 'PA' ? 'Putra' : 'Putri';
            })
            ->editColumn('kota' , function($datas){
                return $datas->kota . ', ' . $datas->provinsi;
            })
            ->editColumn('tgl_lahir' , function($datas){
                $helper = new HelpersController;
                return $datas->tmp_lahir . ', ' . $helper->konversiTgl($datas->tgl_lahir, 'T');
            })
            ->addColumn('link' , function($datas){
                $links = '<div class="btn-group">';
                $links .= '<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i> Aksi</button>';
                $links .= '<ul class="dropdown-menu pull-right" role="menu">';

                $session = Session::get('usergroup_aktif');
                if ($session == '3' || $session == '1') {
                    $links .= '<li><a href="' . url('/admin/profil-calon-santri', $datas->no_pendaftaran) . '"><i class="fa fa-user"></i> Profil Calon Santri</a></li>';
                    $links .= '<li><a href="' . url('/admin/validasi-dokumen', $datas->no_pendaftaran) . '"><i class="fa fa-check-circle"></i> Validasi Dokumen</a></li>';
                }

                if($session == '2' || $session == '1') {
                    $links .= '<li><a href="' . url('/admin/validasi-pembayaran', $datas->no_pendaftaran) . '"><i class="fa fa-check-circle"></i> Validasi Bukti Pembayaran</a></li>';
                }

                if ($session == '3' || $session == '1') {
                    $links .= '<li><a href="' . url('/admin/set-undangan', $datas->no_pendaftaran) . '"><i class="fa fa-calendar-check-o"></i> Buat Undangan Seleksi</a></li>';
                }
                $links .= '</ul>';
                $links .= '</div>';

                return $links;
            })
            ->rawColumns(['link'])
            ->toJson();
    }

    public function profilCalonSantri($noPendaftaran)
    {
        $data = DB::table('psb_akd_csntr as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->leftJoin('psb_ref_jenjang as e', 'a.id_jenjang', '=', 'e.id_jenjang')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi', 'e.nama as jenjang')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtSekolah = DB::table('psb_akd_csntr_sekolah_asal as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtAyah = DB::table('psb_akd_csntr_ayah as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtIbu = DB::table('psb_akd_csntr_ibu as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $konten = HelperDashboard::getSettingValue('konten-sp3k');

        return view('admin.dataCalonSantri.profil', compact('data', 'dtSekolah', 'dtAyah', 'dtIbu', 'konten'));
    }

    public function export()
    {
        $thnAkd = Session::get('thn_akd');
        $gelombang = Session::get('gelombang');

        $datas = DB::table('view_calon_santri_lengkap')
            ->where("thn_akd", "=", "$thnAkd")
            ->where("gelombang", "=", "$gelombang")
            ->orderBy("id_jenjang", "ASC")
            ->get();

        return view('admin.dataCalonSantri.export', compact('thnAkd', 'gelombang', 'datas'));
    }

    public function validasiDokumen($noPendaftaran)
    {
        $dtForUpload = DB::table('psb_akd_csntr_dokumen as a')
            ->leftJoin('psb_ref_dokumen as b', 'a.id_dokumen', '=', 'b.id')
            ->select('a.*', 'b.nama as nm_dokumen')
            ->where('a.no_pendaftaran', '=', "$noPendaftaran")
            ->where('file', '!=', '')
            ->get();

        $dtRefDokumen = PsbRefDokumen::orderBy('orders', 'ASC')->get();

        $dtGeneral = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', "$noPendaftaran")
            ->first();

        $arrDokumenUploaded = [];
        foreach ($dtForUpload as $key => $value) {
            $arrDokumenUploaded[$value->id_dokumen] = $value->file;
        }

        return view('admin.dataCalonSantri.validasiDokumen', compact('arrDokumenUploaded', 'dtGeneral', 'dtRefDokumen'));
    }

    public function doValidasiDokumen(Request $request, $noPendaftaran)
    {
        $data = PsbAkdCsntr::find($noPendaftaran);
        $data->status_upload_dokumen = $request->get('status_upload_dokumen');
        $data->catatan_upload_dokumen = $request->get('catatan_upload_dokumen');
        $data->update();

        return redirect()->back()->with('success', 'Dokumen berhasil divalidasi.');
    }

    public function validasiPembayaran($noPendaftaran)
    {
        $dtGeneral = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        return view('admin.dataCalonSantri.validasiPembayaran', compact('dtGeneral'));
    }

    public function doValidasiPembayaran(Request $request, $noPendaftaran)
    {
        $data = PsbAkdCsntr::find($noPendaftaran);
        $data->konfirmasi_bukti_pembayaran = $request->get('konfirmasi_bukti_pembayaran');
        $data->update();

        return redirect()->back()->with('success', 'Bukti pembayaran berhasil divalidasi.');
    }

    public function setUndangan($noPendaftaran)
    {
        $dtGeneral = DB::table('psb_akd_csntr')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        return view('admin.dataCalonSantri.setUndangan', compact('dtGeneral'));
    }

    public function doSetUndangan(Request $request, $noPendaftaran)
    {
        $data = PsbAkdCsntr::find($noPendaftaran);
        $data->tes_tgl = $request->get('tes_tgl');
        $data->tes_jam = $request->get('tes_jam');
        $data->tes_tempat = $request->get('tes_tempat');
        $data->tes_materi = $request->get('tes_materi');
        $data->update();

        return redirect()->back()->with('success', 'Setting undangan berhasil dibuat.');
    }
}
