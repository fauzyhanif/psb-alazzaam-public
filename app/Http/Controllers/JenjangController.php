<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PsbRefJenjang;

class JenjangController extends Controller
{
    public function index()
    {
        $datas = PsbRefJenjang::all();
        return view('admin.referensi.jenjang.index', compact('datas'));
    }

    public function formAdd(Request $request)
    {
        return view('admin.referensi.jenjang.add');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'id_jenjang'=>'required|integer|unique:psb_ref_jenjang',
            'nama'=>'required'
        ]);

        $new_data = new PsbRefJenjang();
        $new_data->id_jenjang = $request->get('id_jenjang');
        $new_data->nama = $request->get('nama');
        $new_data->save();

        return redirect('/jenjang')->with('success', 'jenjang berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data = PsbRefJenjang::where('id_jenjang', '=', $id)->first();
        return view('admin.referensi.jenjang.edit', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'id_jenjang'=>'required|integer|unique:psb_ref_jenjang,id_jenjang,' . $id . ',id_jenjang',
            'nama'=>'required'
        ]);

        $data = PsbRefJenjang::find($id);
        $data->id_jenjang = $request->get('id_jenjang');
        $data->nama = $request->get('nama');
        $data->is_aktif = $request->get('is_aktif');
        $data->update();

        return redirect('/jenjang')->with('success', 'jenjang berhasil diubah');
    }

    public function delete($id)
    {
        $data = PsbRefJenjang::find($id);
        $data->delete();

        return redirect('/jenjang')->with('success', 'jenjang berhasil dihapus');
    }
}
