<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PsbRefSesi;
use App\PsbRefJenjang;

class SesiController extends Controller
{
    public function index()
    {
        $datas = PsbRefSesi::orderBy("tgl_mulai", "desc")->get();
        return view('admin.referensi.sesi.index', compact('datas'));
    }
    public function formAdd(Request $request)
    {
        return view('admin.referensi.sesi.add');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'thn_akd'=>'required',
            'gelombang'=>'required',
            'tgl_mulai'=>'required',
            'tgl_selesai'=>'required',
            'biaya'=>'required',
            'tes_tgl'=>'required',
            'tes_jam'=>'required',
            'tes_tempat'=>'required',
            'tes_materi'=>'required'
        ]);

        PsbRefSesi::where('is_aktif', 'Y')->update(['is_aktif' => 'N']);

        $new_data = new PsbRefSesi();
        $new_data->thn_akd = $request->get('thn_akd');
        $new_data->gelombang = $request->get('gelombang');
        $new_data->tgl_mulai = $request->get('tgl_mulai');
        $new_data->tgl_selesai = $request->get('tgl_selesai');
        $new_data->biaya = str_replace([".", ","], "", $request->get('biaya'));
        $new_data->tes_jam = $request->get('tes_jam');
        $new_data->tes_tempat = $request->get('tes_tempat');
        $new_data->tes_materi = $request->get('tes_materi');
        $new_data->save();

        return redirect()->back()->with('success', 'Sesi berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data = PsbRefSesi::where('id', '=', $id)->first();
        return view('admin.referensi.sesi.edit', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'thn_akd'=>'required',
            'gelombang'=>'required',
            'tgl_mulai'=>'required',
            'tgl_selesai'=>'required',
            'biaya'=>'required',
            'tes_tgl'=>'required',
            'tes_jam'=>'required',
            'tes_tempat'=>'required',
            'tes_materi'=>'required',
        ]);

        $data = PsbRefSesi::find($id);
        $data->thn_akd = $request->get('thn_akd');
        $data->gelombang = $request->get('gelombang');
        $data->tgl_mulai = $request->get('tgl_mulai');
        $data->tgl_selesai = $request->get('tgl_selesai');
        $data->biaya = str_replace([".", ","], "", $request->get('biaya'));
        $data->tes_tgl = $request->get('tes_tgl');
        $data->tes_jam = $request->get('tes_jam');
        $data->tes_tempat = $request->get('tes_tempat');
        $data->tes_materi = $request->get('tes_materi');
        $data->is_aktif = $request->get('is_aktif');
        $data->update();

        return redirect()->back()->with('success', 'Sesi berhasil diubah');
    }

    public function delete($id)
    {
        $data = PsbRefSesi::find($id);
        $data->deleted_at = date('Y-m-d H:i:s');
        $data->save();
        
        return redirect('/sesi')->with('success', 'Sesi berhasil dihapus');
    }
}
