<?php

namespace App\Http\Controllers;

use App\Helpers\HelperDashboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
class DownloadBerkasController extends Controller
{
    public function buktiPendaftaran($noPendaftaran)
    {
        $dtGeneral = DB::table('psb_akd_csntr as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->leftJoin('psb_ref_jenjang as e', 'a.id_jenjang', '=', 'e.id_jenjang')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi', 'e.nama as jenjang')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtSekolah = DB::table('psb_akd_csntr_sekolah_asal as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtAyah = DB::table('psb_akd_csntr_ayah as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $dtIbu = DB::table('psb_akd_csntr_ibu as a')
            ->leftJoin('sys_ref_jenjang_pendidikan as b', 'a.id_pendidikan', '=', 'b.id')
            ->leftJoin('sys_ref_pekerjaan as c', 'a.id_pekerjaan', '=', 'c.id')
            ->leftJoin('sys_ref_penghasilan as d', 'a.id_penghasilan', '=', 'd.id_penghasilan')
            ->select('a.*', 'b.nama as pendidikan', 'c.nama as pekerjaan', 'd.nm_penghasilan as penghasilan')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $pdf = PDF::loadView('downloadBerkas.buktiPendaftaran.print', compact('dtGeneral', 'dtSekolah', 'dtAyah', 'dtIbu'));
        return $pdf->stream($noPendaftaran.'-BUKTI-PENDAFTARAN.pdf');
    }

    public function lanjutanPendaftaran($noPendaftaran)
    {
        $data = DB::table('psb_akd_csntr')->where('no_pendaftaran', '=', $noPendaftaran)->first();

        $jenjang = '';
        if ($data->id_jenjang == 1) {
            $jenjang = 'smp';
        } elseif ($data->id_jenjang == 2) {
            $jenjang = 'im';
        }  elseif ($data->id_jenjang == 3) {
            $jenjang = 'sma';
        } elseif ($data->id_jenjang == 4) {
            $jenjang = 'sma';
        }

        $pdf = PDF::loadView('downloadBerkas.lanjutanPendaftaran.' . $jenjang, compact('data'));
        return $pdf->stream($noPendaftaran.'-LANJUTAN-PENDAFTARAN.pdf');
    }

    public function kesanggupan($noPendaftaran)
    {
        $dtGeneral = DB::table('psb_akd_csntr as a')
            ->leftJoin('sys_ref_data_wilayah as b', 'a.id_kecamatan', '=', 'b.id_wil')
            ->leftJoin('sys_ref_data_wilayah as c', 'a.id_kota', '=', 'c.id_wil')
            ->leftJoin('sys_ref_data_wilayah as d', 'a.id_provinsi', '=', 'd.id_wil')
            ->leftJoin('psb_akd_csntr_ayah as e', 'a.no_pendaftaran', '=', 'e.no_pendaftaran')
            ->leftJoin('psb_ref_jenjang as f', 'a.id_jenjang', '=', 'f.id_jenjang')
            ->select('a.*', 'b.nm_wil as kecamatan', 'c.nm_wil as kota', 'd.nm_wil as provinsi', 'e.nama as nm_ayah', 'e.no_hp', 'f.nama as nm_jenjang')
            ->where('a.no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $thnAkd = $dtGeneral->thn_akd;
        $konten = HelperDashboard::getSettingValue('konten-sp3k');

        $pdf = PDF::loadView('downloadBerkas.kesanggupan.umum', compact('dtGeneral', 'thnAkd', 'konten'));
        return $pdf->stream('SURAT-KESANGGUPAN.pdf');
    }

    public function undangan($noPendaftaran)
    {
        $data = DB::table('psb_akd_csntr as a')
            ->leftJoin('psb_ref_jenjang as b', 'a.id_jenjang', '=', 'b.id_jenjang')
            ->select('a.*', 'b.nama as nm_jenjang')
            ->where('no_pendaftaran', '=', $noPendaftaran)
            ->first();

        $thnAkd = $data->thn_akd;

        $pdf = PDF::loadView('downloadBerkas.undanganSeleksi.umum', compact('data', 'thnAkd'));
        return $pdf->stream($noPendaftaran.'-UNDANGAN-SELEKSI.pdf');
    }

}
