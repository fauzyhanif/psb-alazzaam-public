<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('username')) {
            if (Session::get('usergroup_aktif') != '4') {
                return $next($request);
            } else {
                return redirect('/access-denied');
            }
        }

        return redirect('/auth');
    }
}
