<?php
namespace App\Helpers;

use App\PsbRefSetting;
use Illuminate\Support\Facades\DB;

class HelperDashboard {
    public static function ThnAkdAktif() {
        $result = 0;
        $data = DB::table('psb_ref_sesi')->where('is_aktif', '=', 'Y')->first();

        if ($data !== null) {
            $result = $data->thn_akd;
        }
        return $result;
    }

    public static function GelombangAktif() {
        $result = 0;
        $data = DB::table('psb_ref_sesi')->where('is_aktif', '=', 'Y')->first();

        if ($data !== null) {
            $result = $data->gelombang;
        }
        return $result;
    }

    public static function TotalPendaftar($jenjang = '*') {
        $helper = new HelperDashboard();
        $thnAkd = substr($helper->ThnAkdAktif(),0,4);
        $gelombang = $helper->GelombangAktif();

        if ($jenjang == '*') {
            $data = DB::table('psb_akd_csntr')
                ->where('thn_akd', '=', $thnAkd)
                ->where('gelombang', '=', $gelombang)
                ->count();
        } else {
            $data = DB::table('psb_akd_csntr')
                ->where('thn_akd', '=', $thnAkd)
                ->where('gelombang', '=', $gelombang)
                ->where('id_jenjang', '=', $jenjang)
                ->count();
        }

        return $data;
    }

    public static function RincianPendaftar($jnsKelamin, $jenjang)
    {
        $helper = new HelperDashboard();
        $thnAkd = substr($helper->ThnAkdAktif(),0,4);
        $gelombang = $helper->GelombangAktif();

        $data = DB::table('psb_akd_csntr')
                ->where('thn_akd', '=', $thnAkd)
                ->where('gelombang', '=', $gelombang)
                ->where('id_jenjang', '=', $jenjang)
                ->where('jns_kelamin', '=', $jnsKelamin)
                ->groupBy('id_jenjang', 'jns_kelamin')
                ->count();

        return $data;
    }

    public static function PendaftarTerakhir()
    {
        $helper = new HelperDashboard();
        $thnAkd = substr($helper->ThnAkdAktif(),0,4);
        $gelombang = $helper->GelombangAktif();

        $data = DB::table('psb_akd_csntr as a')
            ->leftJoin('psb_ref_jenjang as b', 'a.id_jenjang', '=', 'b.id_jenjang')
            ->select('a.no_pendaftaran', 'a.nama', 'b.nama as jenjang')
            ->where('a.thn_akd', '=', $thnAkd)
            ->where('a.gelombang', '=', $gelombang)
            ->orderBy('a.id', 'desc')
            ->limit(5)
            ->get();

        return $data;
    }

    public static function bulanRomawi($bulan)
    {
        $arr['01'] = 'I';
        $arr['02'] = 'II';
        $arr['03'] = 'III';
        $arr['04'] = 'IV';
        $arr['05'] = 'V';
        $arr['06'] = 'VI';
        $arr['07'] = 'VII';
        $arr['08'] = 'VIII';
        $arr['09'] = 'IX';
        $arr['10'] = 'X';
        $arr['11'] = 'XI';
        $arr['12'] = 'XII';

        return $arr[$bulan];
    }

    public static function getSettingValue($parameter = null)
    {
        $res = '';
        $data = PsbRefSetting::where('parameter', $parameter)->first();
        
        if ($data) {
            $res = $data->value;
        }

        return $res;
    }
}
?>
