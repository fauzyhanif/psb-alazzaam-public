<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class HelperDataReferensi {

    public static function ThnAkdAktif() {
        $result = 0;
        $this_day = date('Y-m-d');
        $data = DB::table('psb_ref_sesi')
                ->where('tgl_mulai', '<=', $this_day)
                ->where('tgl_selesai', '>=', $this_day)
                ->where('is_aktif', '=', 'Y')
                ->first();

        if ($data !== null) {
            $result = $data->thn_akd;
        }

        return $result;
    }

    public static function GelombangAktif() {
        $result = 0;
        $this_day = date('Y-m-d');
        $data = DB::table('psb_ref_sesi')
                ->where('tgl_mulai', '<=', $this_day)
                ->where('tgl_selesai', '>=', $this_day)
                ->where('is_aktif', '=', 'Y')
                ->first();

        if ($data !== null) {
            $result = $data->gelombang;
        }
        return $result;
    }

    public static function BiayaAktif() {
        $result = 0;
        $data = DB::table('psb_ref_sesi')
                ->where('is_aktif', '=', 'Y')
                ->first();

        if ($data !== null) {
            $result = $data->biaya;
        }

        return $result;
    }

    public static function getJadwalTes()
    {
        $data = DB::table('psb_ref_sesi')
                ->select('tes_tgl', 'tes_jam', 'tes_tempat', 'tes_materi')
                ->where('is_aktif', '=', 'Y')
                ->first();

        if ($data !== null) {
            return $data;
        }
    }

    public static function DtProvinsi() {
        $data = DB::table('sys_ref_data_wilayah')
                ->where('id_level_wil', '=', '1')
                ->get();

        return $data;
    }

    public static function DtKotaByProvinsi($idInduk) {
        $data = DB::table('sys_ref_data_wilayah')
            ->where('id_induk_wilayah', '=', "$idInduk")
            ->orderBy('nm_wil')
            ->get();

        return $data;
    }

    public static function DtKecamatanByKota($idInduk) {
        $data = DB::table('sys_ref_data_wilayah')
            ->where('id_induk_wilayah', '=', "$idInduk")
            ->orderBy('nm_wil')
            ->get();

        return $data;
    }

    public static function DtJenjang() {
        $data = DB::table('psb_ref_jenjang')
                ->where('is_aktif', '=', 'Y')
                ->get();

        return $data;
    }

    public static function DtPendidikan() {
        $data = DB::table('sys_ref_jenjang_pendidikan')
                ->where('jenjang_orang', '=', '1')
                ->get();

        return $data;
    }

    public static function DtPenghasilan() {
        $data = DB::table('sys_ref_penghasilan')->get();

        return $data;
    }

    public static function DtPekerjaan() {
        $data = DB::table('sys_ref_pekerjaan')->get();

        return $data;
    }

    public static function konversiTgl($date, $date_format='')
    {
        $dayList = array(
            'Sunday'    => 'Minggu',
            'Monday'    => 'Senin',
            'Tuesday'   => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday'  => 'Kamis',
            'Friday'    => 'Jumat',
            'Saturday'  => 'Sabtu'
        );

        $monthList = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );

        $format_hari = date('l', strtotime($date));
        $format_tgl  = date('d', strtotime($date));
        $format_bln  = date('m', strtotime($date));
        $format_thn  = date('Y', strtotime($date));

        switch ($date_format) {
            case 'l':
                # Hari ex: Kamis
                $output = $dayList[$format_hari];
                break;
            case 'd':
                # Tanggal ex: 21
                $output = $format_tgl;
                break;
            case 'm':
                # Bulan ex: Januari
                $output = $monthList[$format_bln];
                break;
            case 'y':
                # Tahun ex: 2016
                $output = $format_thn;
                break;
            case 'T':
                # Tgl Lahir
                $output = $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
            default:
                # Hari, Tanggal-Bulan-Tahun ex: Rabu, 26-Juli-2016
                $output = $dayList[$format_hari] . ', ' . $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
        }

        return $output;
    }

    public static function nomorSuratLanjutanPendaftaran($jenjang)
    {
        $bulan = date('m');
        $tahun = date('Y');

        $bulanRomawi = [
            "01" => "I",
            "02" => "II",
            "03" => "III",
            "04" => "IV",
            "05" => "V",
            "06" => "VI",
            "07" => "VII",
            "08" => "VIII",
            "09" => "IX",
            "10" => "X",
            "11" => "XI",
            "12" => "XII",
        ];

        $nomor_awal = ($jenjang == '1') ? '001' : '002';
        $nomor_surat = $nomor_awal . "/Pan-PSB/" . $bulanRomawi[$bulan] . "/" . $tahun;
        return $nomor_surat;
    }
}
