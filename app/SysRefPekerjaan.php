<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRefPekerjaan extends Model
{
    protected $table = "sys_ref_pekerjaan";
    protected $primaryKey = "id";
    protected $fiilable = [
        "id",
        "nama",
    ];
}
