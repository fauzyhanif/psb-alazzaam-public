<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PsbRefDokumen extends Model
{
    protected $table = "psb_ref_dokumen";
    protected $primaryKey = "id";
    protected $fiilable = [
        "id",
        "nama",
    ];
}
