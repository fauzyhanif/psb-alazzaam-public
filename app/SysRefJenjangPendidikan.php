<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRefJenjangPendidikan extends Model
{
    protected $table = "sys_ref_jenjang_pendidikan  ";
    protected $primaryKey = "id";
    protected $fiilable = [
        "id",
        "nama",
        "jenjang_lembaga",
        "jenjang_orang",
    ];
}
