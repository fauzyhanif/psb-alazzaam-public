@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Pilih Usergroup
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Silahkan pilih Usergroup</h3>
                </div>
                <div class="box-body">
                    <?php
                        $r = array_filter(explode(',', $sessionUsergroup));
                        foreach ($dtUsergroup as $ug){
                            if (in_array($ug->id_usergroup, $r)){
                    ?>
                        <a href="{{ url('set-usergroup', $ug->id_usergroup) }}" class="btn btn-block btn-social btn-dropbox">
                            <i class='fa fa-users'></i> {{ $ug->nama }}
                        </a>
                    <?php }} ?>
                </div>
              </div>
        </div>
    </div>
</section>
@endsection
