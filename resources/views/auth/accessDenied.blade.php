@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Pilih Usergroup
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <h2 class="text-red" style="font-size: 56px;"><i class="fa fa-warning text-red"></i> 500</h2>

            <h3>Mohon maaf, anda tidak mempunyai akses ke halaman ini.</h3>
        </div>
    </div>
</section>
@endsection
