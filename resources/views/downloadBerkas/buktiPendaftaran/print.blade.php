<!DOCTYPE html>
<html>

    <head>
        <title>Bukti Pendaftaran</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('downloadBerkas.css');
    </head>

    <body>
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-md-12">                
                    <img src="{{ url('/public/img/kop-surat-all.png') }}" alt="Kop Surat" style="width: 100%;">
                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-12">
                    <h5>FORMULIR PENDAFTARAN SANTRI BARU <br> TAHUN AJARAN {{ $dtGeneral->thn_akd }}/{{ $dtGeneral->thn_akd + 1 }}</h5>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5"> </div>
                <div class="col-md-2 text-center" style="border: 1px solid grey; width: 12%; height: 120px; margin: auto;">
                    <span style="margin: auto">Pas foto 3x4</span>
                </div>
                <div class="col-md-5"> </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <b class="data-title">A. Nomor Pendaftaran</b>
                    <table class="table table-sm borderless">
                        <tbody>
                            <tr>
                                <td><h4><b>{{ $dtGeneral->no_pendaftaran }}</b></h4></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <b class="data-title">B. Jenjang Tujuan</b>
                    <table class="table table-sm borderless">
                        <tbody>
                            <tr>
                                <td width="30%">Jenjang/ Unit Pendidikan Tujuan</td>
                                <td width="70%">: <b>{{ $dtGeneral->jenjang }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <b class="data-title">C. Data Pribadi</b>
                    <table class="table table-sm borderless">
                        <tbody>
                            <tr>
                                <td width="30%">Nama Lengkap</td>
                                <td width="70%">: <b>{{ $dtGeneral->nama }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Username <span class="text-blue"><i>(Untuk login ke sistem)</i></span>
                                </td>
                                <td width="70%">: <b>{{ $dtGeneral->no_pendaftaran }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Password <span class="text-blue"><i>(Untuk login ke sistem)</i></span>
                                </td>
                                <td width="70%">: <b>{{ $dtGeneral->password }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">NIK</td>
                                <td width="70%">: <b>{{ $dtGeneral->nik }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">No Kartu Keluarga (KK)</td>
                                <td width="70%">: <b>{{ $dtGeneral->kk }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">NISN</td>
                                <td width="70%">: <b>{{ $dtGeneral->nisn }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Jenis Kelamin</td>
                                <td width="70%">: <b>{{ $dtGeneral->jns_kelamin == 'PA' ? 'LAKI-LAKI' : 'PEREMPUAN' }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">TTL</td>
                                <td width="70%">:
                                    <b>
                                        {{ $dtGeneral->tmp_lahir }},
                                        {{ HelperDataReferensi::konversiTgl($dtGeneral->tgl_lahir, 'T') }}
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">Anak ke</td>
                                <td width="70%">: <b>{{ $dtGeneral->anak_ke != NULL ? $dtGeneral->anak_ke : '-' }} dari {{ $dtGeneral->jml_sdr }} bersaudara</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">Golongan Darah</td>
                                <td width="70%">: <b>{{ $dtGeneral->gol_darah }}</b></td>
                            </tr>
                            </tr>
                            <tr>
                                <td width="30%">Alamat</td>
                                <td width="70%">: <b>{{ $dtGeneral->alamat }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">RT/RW</td>
                                <td width="70%">: <b>{{ $dtGeneral->rt }}/{{ $dtGeneral->rw }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Desa / Kelurahan</td>
                                <td width="70%">: <b>{{ $dtGeneral->id_desa }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kecamatan</td>
                                <td width="70%">: <b>{{ $dtGeneral->kecamatan }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kota / Kabupaten</td>
                                <td width="70%">: <b>{{ $dtGeneral->kota }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Provinsi</td>
                                <td width="70%">: <b>{{ $dtGeneral->provinsi }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kode Pos</td>
                                <td width="70%">: <b>{{ $dtGeneral->kode_pos != NULL ? $dtGeneral->kode_pos : '-' }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">No. KIP (Jika ada)</td>
                                <td width="70%">: <b>{{ $dtGeneral->pip }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <b class="data-title">D. Data Sekolah Asal</b>
                    <table class="table table-sm borderless">
                        <tbody>
                            <tr>
                                <td width="30%">Nama Sekolah</td>
                                <td width="70%">: <b>{{ $dtSekolah->nama }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">NPSN/NSM</td>
                                <td width="70%">: <b>{{ $dtSekolah->nspn }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kecamatan</td>
                                <td width="70%">: <b>{{ $dtSekolah->kecamatan }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kota</td>
                                <td width="70%">: <b>{{ $dtSekolah->kota }}</b></td>
                            </tr>
                            
                            @if ($dtGeneral->id_jenjang == '1')
                                <tr>
                                    <td colspan="2"><b>Nilai Semester 1 Kelas 5 SD</b></td>
                                </tr>
                            @endif

                            @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
                                <tr>
                                    <td colspan="2"><b>Nilai Semester 1 Kelas 8 SMP</b></td>
                                </tr>
                            @endif
                            
                            <tr>
                                <td width="30%">Nilai Bhs Indonesia</td>
                                <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_indonesia }}</b></td>
                            </tr>
                            
                            @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
                                <tr>
                                    <td width="30%">Nilai Bhs Inggris</td>
                                    <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_inggris }}</b></td>
                                </tr>
                            @endif
                            
                            <tr>
                                <td width="30%">Nilai Matematika</td>
                                <td width="70%">: <b>{{ $dtSekolah->nilai_matematika }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Nilai Ipa</td>
                                <td width="70%">: <b>{{ $dtSekolah->nilai_ipa }}</b></td>
                            </tr>

                            @if ($dtGeneral->id_jenjang == '1')
                                <tr>
                                    <td colspan="2"><b>Nilai Semester 2 Kelas 5 SD</b></td>
                                </tr>
                            @endif

                            @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
                                <tr>
                                    <td colspan="2"><b>Nilai Semester 2 Kelas 8 SMP</b></td>
                                </tr>
                            @endif
                            
                            <tr>
                                <td width="30%">Nilai Bhs Indonesia</td>
                                <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_indonesia_2 }}</b></td>
                            </tr>
                            
                            @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
                                <tr>
                                    <td width="30%">Nilai Bhs Inggris</td>
                                    <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_inggris_2 }}</b></td>
                                </tr>
                            @endif
                            
                            <tr>
                                <td width="30%">Nilai Matematika</td>
                                <td width="70%">: <b>{{ $dtSekolah->nilai_matematika_2 }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Nilai Ipa</td>
                                <td width="70%">: <b>{{ $dtSekolah->nilai_ipa_2 }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <b class="data-title">E. Data Ayah</b>
                    <table class="table table-sm borderless">
                        <tbody>
                            <tr>
                                <td width="30%">Nama Lengkap</td>
                                <td width="70%">: <b>{{ $dtAyah->nama }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">NIK</td>
                                <td width="70%">: <b>{{ $dtAyah->nik }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">TTL</td>
                                <td width="70%">:
                                    <b>{{ $dtAyah->tmp_lahir . ', ' . HelperDataReferensi::konversiTgl($dtAyah->tgl_lahir, 'T') }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">Pendidikan terakhir</td>
                                <td width="70%">: <b>{{ $dtAyah->pendidikan }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Pekerjaan</td>
                                <td width="70%">: <b>{{ $dtAyah->pekerjaan }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">No HP/WA</td>
                                <td width="70%">: <b>{{ $dtAyah->no_hp }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Status</td>
                                <td width="70%">: <b>{{ $dtAyah->status_hidup }}</b></td>
                            </tr>
                            {{-- <tr>
                                <td width="30%">Penghasilan</td>
                                <td width="70%">: <b>{{ $dtAyah->penghasilan }}</b></td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <b class="data-title">F. Data Ibu</b>
                    <table class="table table-sm borderless">
                        <tbody>
                            <tr>
                                <td width="30%">Nama Lengkap</td>
                                <td width="70%">: <b>{{ $dtIbu->nama }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">NIK</td>
                                <td width="70%">: <b>{{ $dtIbu->nik }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">TTL</td>
                                <td width="70%">:
                                    <b>{{ $dtIbu->tmp_lahir . ', ' . HelperDataReferensi::konversiTgl($dtIbu->tgl_lahir, 'T') }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">Pendidikan terakhir</td>
                                <td width="70%">: <b>{{ $dtIbu->pendidikan }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Pekerjaan</td>
                                <td width="70%">: <b>{{ $dtIbu->pekerjaan }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">No HP/WA</td>
                                <td width="70%">: <b>{{ $dtIbu->no_hp }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Status Hidup</td>
                                <td width="70%">: <b>{{ $dtIbu->status_hidup }}</b></td>
                            </tr>
                            {{-- <tr>
                                <td width="30%">Penghasilan</td>
                                <td width="70%">: <b>{{ $dtIbu->penghasilan }}</b></td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <b class="data-title">G. Data Lain-lain</b>
                    <table class="table table-sm borderless">
                        <tbody>
                            <tr>
                                <td width="30%">
                                    Penanggung Jawab Biaya pendidikan santri/santriwati
                                </td>
                                <td width="70%">: <b>{{ $dtGeneral->pj_biaya }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Siapkah di uji hafalan qur’an 5 juz?</td>
                                <td width="70%">: <b>{{ $dtGeneral->siap_diuji_hafalan }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 200px;">
                    <p>Semarang, {{ HelperDataReferensi::konversiTgl(date('Y-m-d'), 'T') }}</p>
                    <p>Orang Tua/Wali</p>

                    <br>
                    <br>
                    <br>
                    <p>({{ $dtAyah->nama }})</p>
                </div>
            </div>
        </div>

    </body>

</html>
