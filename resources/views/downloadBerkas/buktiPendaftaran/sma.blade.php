<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .borderless {
            font-size: 14px;
            border-collapse: separate;
            border-spacing:0 -15px;
            margin-top: 10px;
        }

        .borderless td, .borderless th {
            border: none;
        }

        .data-title {
            font-size: 16px;
        }

    </style>
</head>
<body>
	<div class="container-fluid">
        <div class="row text-center">
            <div class="col-md-12">
                <img src="{{ url('/public/img/kop-surat.png') }}" alt="Kop Surat" style="width: 100%; height: 140px;">
            </div>
        </div>

        <div class="row text-center">
            <div class="col-md-12">
                <h4>Bukti Pendaftaran</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <b class="data-title">A. Data Pribadi</b>
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="30%">No Pendaftaran</td>
                            <td width="70%">: <b>{{ $dtGeneral->no_pendaftaran }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Nama</td>
                            <td width="70%">: <b>{{ $dtGeneral->nama }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Username <span class="text-blue"><i>(Untuk login ke sistem)</i></span></td>
                            <td width="70%">: <b>{{ $dtGeneral->no_pendaftaran }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Password <span class="text-blue"><i>(Untuk login ke sistem)</i></span></td>
                            <td width="70%">: <b>{{ $dtGeneral->password }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">NISN</td>
                            <td width="70%">: <b>{{ $dtGeneral->nisn }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Jenjang Tujuan</td>
                            <td width="70%">: <b>{{ $dtGeneral->jenjang }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Jenis Kelamin</td>
                            <td width="70%">: <b>{{ $dtGeneral->jns_kelamin == 'PA' ? 'PUTRA' : 'PUTRI' }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">TTL</td>
                        <td width="70%">: <b>{{ $dtGeneral->tmp_lahir }}, {{ HelperDataReferensi::konversiTgl($dtGeneral->tgl_lahir, 'T') }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Jumlah Hafalan</td>
                            <td width="70%">: <b>{{ $dtGeneral->jml_hafalan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Anak ke</td>
                            <td width="70%">: <b>{{ $dtGeneral->anak_ke != NULL ? $dtGeneral->anak_ke : '-' }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Jumlah Saudara</td>
                            <td width="70%">: <b>{{ $dtGeneral->jml_sdr != NULL ? $dtGeneral->jml_sdr : '-' }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Golongan Darah</td>
                            <td width="70%">: <b>{{ $dtGeneral->gol_darah }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Alamat</td>
                            <td width="70%">: <b>{{ $dtGeneral->alamat }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">RT/RW</td>
                            <td width="70%">: <b>{{ $dtGeneral->rt }}/{{ $dtGeneral->rw }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Desa</td>
                            <td width="70%">: <b>{{ $dtGeneral->id_desa }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Kecamatan</td>
                            <td width="70%">: <b>{{ $dtGeneral->kecamatan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Kota</td>
                            <td width="70%">: <b>{{ $dtGeneral->kota }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Provinsi</td>
                            <td width="70%">: <b>{{ $dtGeneral->provinsi }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Kode Pos</td>
                            <td width="70%">: <b>{{ $dtGeneral->kode_pos != NULL ? $dtGeneral->kode_pos : '-' }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <b class="data-title">B. Data Sekolah Asal</b>
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="30%">Nama Sekolah</td>
                            <td width="70%">: <b>{{ $dtSekolah->nama }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">NSPN</td>
                            <td width="70%">: <b>{{ $dtSekolah->nspn }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Tahun Lulus</td>
                            <td width="70%">: <b>{{ $dtSekolah->thn_lulus }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Alamat</td>
                            <td width="70%">: <b>{{ $dtSekolah->alamat }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Desa</td>
                            <td width="70%">: <b>{{ $dtSekolah->id_desa }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Kecamatan</td>
                            <td width="70%">: <b>{{ $dtSekolah->kecamatan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Kota</td>
                            <td width="70%">: <b>{{ $dtSekolah->kota }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Provinsi</td>
                            <td width="70%">: <b>{{ $dtSekolah->provinsi }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Kode Pos</td>
                            <td width="70%">: <b>{{ $dtSekolah->kode_pos != NULL ? $dtSekolah->kode_pos : '-' }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <b class="data-title">C. Data Ayah</b>
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="30%">Nama</td>
                            <td width="70%">: <b>{{ $dtAyah->nama }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Nama</td>
                            <td width="70%">: <b>{{ $dtAyah->no_hp }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Status Hidup</td>
                            <td width="70%">: <b>{{ $dtAyah->status_hidup }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">NIK</td>
                            <td width="70%">: <b>{{ $dtAyah->nik }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">TTL</td>
                            <td width="70%">: <b>{{ $dtAyah->tmp_lahir . ', ' . HelperDataReferensi::konversiTgl($dtAyah->tgl_lahir, 'T') }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Pendidikan terakhir</td>
                            <td width="70%">: <b>{{ $dtAyah->pendidikan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Pendidikan terakhir</td>
                            <td width="70%">: <b>{{ $dtAyah->pendidikan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Pekerjaan</td>
                            <td width="70%">: <b>{{ $dtAyah->pekerjaan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Penghasilan</td>
                            <td width="70%">: <b>{{ $dtAyah->penghasilan }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <b class="data-title">D. Data Ibu</b>
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="30%">Nama</td>
                            <td width="70%">: <b>{{ $dtIbu->nama }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Nama</td>
                            <td width="70%">: <b>{{ $dtIbu->no_hp }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Status Hidup</td>
                            <td width="70%">: <b>{{ $dtIbu->status_hidup }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">NIK</td>
                            <td width="70%">: <b>{{ $dtIbu->nik }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">TTL</td>
                            <td width="70%">: <b>{{ $dtIbu->tmp_lahir . ', ' . HelperDataReferensi::konversiTgl($dtIbu->tgl_lahir, 'T') }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Pendidikan terakhir</td>
                            <td width="70%">: <b>{{ $dtIbu->pendidikan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Pendidikan terakhir</td>
                            <td width="70%">: <b>{{ $dtIbu->pendidikan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Pekerjaan</td>
                            <td width="70%">: <b>{{ $dtIbu->pekerjaan }}</b></td>
                        </tr>
                        <tr>
                            <td width="30%">Penghasilan</td>
                            <td width="70%">: <b>{{ $dtIbu->penghasilan }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <b class="data-title">E. Data Lain-lain</b>
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="30%">Penanggung Jawab Biaya</td>
                            <td width="70%">: <b>{{ $dtGeneral->pj_biaya }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-center" style="margin-left: 400px;">
                <p>Semarang, {{ HelperDataReferensi::konversiTgl(date('Y-m-d'), 'T') }}</p>
                <p>Orang Tua/Wali</p>

                <br>
                <br>
                <br>
                <p>({{ $dtAyah->nama }})</p>
            </div>
        </div>
    </div>

</body>
</html>
