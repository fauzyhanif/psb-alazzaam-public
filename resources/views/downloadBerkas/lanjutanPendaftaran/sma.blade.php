<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @include('downloadBerkas.css');
</head>
<body>
	<div class="container-fluid" style="padding: 0px 20px;">
        <div class="row text-center">
            <img src="{{ url('/public/img/kop-surat-all.png') }}" alt="Kop Surat"
                style="width: 100%;">
        </div>

        <div class="row text-center">
            <div class="col-md-12">
                <h4>Lanjutan Pendaftaran</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="ket-surat">
                    <p>
                        No. : {{ substr($data->no_pendaftaran, -3) }}/{{ substr($data->no_pendaftaran,0,2) }}/Pan-PSB/{{ HelperDashboard::bulanRomawi(substr($data->tgl_daftar,5,2)) }}/{{ substr($data->tgl_daftar,0,4) }}
                    </p>
                    <p>
                        Hal : Informasi Lanjut Pendaftaran
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="target-surat">
                    <p>Kepada Ykh. Calon Santri dan Orang Tua/Wali</p>
                    <p>Di Tempat</p>
                </div>
            </div>
        </div>

        <div class="row text-center">
            <div class="col-md-12">
                <div class="salam">
                    <i>Assalamu 'alaikum warahmatullahi wabarakatuh</i>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>
                    Alhamdulillah, shalawat dan salam semoga tercurah pada Rasulullah shallallahu 'alaihi wasallam, keluarga, para sahabat
                    dan pengikutnya hingga hari akhir kelak.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>
                    Proses pendaftaran online Ananda sudah selesai mengisi Formulir Pendaftaran dan dinyatakan berhasil dan sudah masuk
                    ke dalam data kami sebagai calon santri baru. Dengan ini kami informasikan bahwa untuk dapat menempuh ujian seleksi,
                    Ananda harus melengkapi berkas syarat pendaftaran sebagai berikut:
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <ol>
                    <li>PAS FOTO 3x4</li>
                    <li>SCAN AKTE KELAHIRAN</li>
                    <li>SCAN KK TERBARU</li>
                    <li>SCAN KTP AYAH & IBU / WALI</li>
                    <li>SCAN SURAT KET. NISN</li>
                    <li>SCAN RAPORT 2 SMT (SMP = KELAS 5, SMA = KELAS 8)</li>
                    <li>SCAN SURAT KESANGGUPAN (SP3K)</li>
                    <li>SCAN FORM PENDAFTARAN</li>
                    <li>SCAN KARTU PIP (jika ada)</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>
                    Seluruh berkas di atas diupload melalui halaman Informasi Santri pada bagian Upload Dokumen,
                    diharapkan segera melakukan transfer uang pendaftaran sebesar Rp @currency($data->biaya),-
                    (3 digit terakhir sesuai nomor pendaftaran diatas) ke Rek. Bank BSI dengan No rekening : 3333444417 An. PPTQ Al
                    Azzaam diiringi dengan konfirmasi melalui Sistem Informasi di menu Upload Bukti Pembayaran.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>
                    Kemudian, hasil upload berkas persyaratan dan bukti transfer uang pendaftaran akan
                    diverifikasi oleh panitia maksimal 2 x 24 jam ke Nomor WA 0812-2718-8101 (Ustadzah Mumun Mulyawati)..
                    Selanjutnya akan diberikan informasi melalui Grup PSB atau Secara Japri Ke Nomor Orang Tua/ Wali
                    calon santri.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>
                    Demikian pemberitahuan ini kami sampaikan, atas perhatiannya kami ucapkan terima kasih,
                    jazakumullahu khayran.
                </p>
            </div>
        </div>

        <div class="row text-center">
            <div class="col-md-12">
                <div class="salam">
                    <i>Wassalamu 'alaikum warahmatullahi wabarakatuh</i>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-center" style="margin-left: 200px;">
                <p>Semarang, {{ HelperDataReferensi::konversiTgl(date('Y-m-d'), 'T') }}</p>
                <p>Hormat kami,</p>
                <p>Ketua Panitia,</p>
                <br>
                <img src="{{ url('/public/img/cap-alazzaam.png') }}" alt="Kop Surat" style="width: 120px; height: 50px;">
                <img src="{{ url('/public/img/ttd-alazzaam.png') }}" alt="Kop Surat" style="width: 100px; height: 35px; padding-left: -80px !important;">
                <br>
                <p>Yusuf Imam Maulana, S.Pd.</p>
            </div>
        </div>

    </div>

</body>
</html>
