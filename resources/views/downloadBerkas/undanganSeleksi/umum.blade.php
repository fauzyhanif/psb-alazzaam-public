<!DOCTYPE html>
<html>

    <head>
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('downloadBerkas.css');
    </head>

    <body>
        <div class="container-fluid">
            <div class="row" style="margin-bottom: -10px;">
                <div class="col-md-12">
                    <img src="{{ url('/public/img/logo_kecil.jpg') }}" alt="Kop Surat" style="width: 40%;">
                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-12">
                    <h4>UNDANGAN TES SELEKSI</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="ket-surat">
                        <p>
                            No. : {{ substr($data->no_pendaftaran, -3) }}/{{ substr($data->no_pendaftaran,0,2)
                            }}/Pan-PSB/{{ HelperDashboard::bulanRomawi(substr($data->tgl_daftar,5,2)) }}/{{
                            substr($data->tgl_daftar,0,4) }}
                        </p>
                        <p>
                            Hal : Informasi Lanjut Pendaftaran
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="target-surat">
                        <p>Kepada Ykh. Calon Santri dan Orang Tua/Wali</p>
                        <p>Di Tempat</p>
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-12">
                    <div class="salam">
                        <i>Assalamu 'alaikum warahmatullahi wabarakatuh</i>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        Sehubungan dengan pendaftaran online/offline calon santri baru PPTQ Al Azzaam yang telah
                        dilakukan oleh Ananda,
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="30%"> Nama</td>
                                <td width="70%">: {{ $data->nama }}</td>
                            </tr>
                            <tr>
                                <td width="30%">No Pendaftaran</td>
                                <td width="70%">: {{ $data->no_pendaftaran }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        maka Panitia Penerimaan Santri Baru PPTQ Al Azzaam TA
                        <?= $thnAkd . '/' . ($thnAkd + 1) ?> mengundang Ananda calon santri beserta orang tua/wali
                        untuk melaksanakan ujian seleksi masuk PPTQ Al Azzaam besok pada:
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="30%"> Hari, Tanggal</td>
                                <td width="70%">: {{ HelperDataReferensi::konversiTgl($data->tes_tgl) }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Waktu</td>
                                <td width="70%">: {{ $data->tes_jam }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Tempat</td>
                                <td width="70%">: {{ $data->tes_tempat }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Materi Ujian</td>
                                <td width="70%">: {{ $data->tes_materi }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        Demikian undangan ini kami sampaikan, atas perhatiannya kami ucapkan terima kasih, jazakumullahu
                        khayran.
                    </p>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-12">
                    <div class="salam">
                        <i>Wassalamu 'alaikum warahmatullahi wabarakatuh</i>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 200px;">
                    <p>Semarang, {{ HelperDataReferensi::konversiTgl(date('Y-m-d'), 'T') }}</p>
                    <p>Hormat kami,</p>
                    <p>Ketua Panitia,</p>
                    <br>
                    <img src="{{ url('/public/img/cap-alazzaam.png') }}" alt="Kop Surat"
                        style="width: 120px; height: 50px;">
                    <img src="{{ url('/public/img/ttd-alazzaam.png') }}" alt="Kop Surat"
                        style="width: 100px; height: 35px; padding-left: -80px !important;">
                    <br>
                    <p>(Yusuf Imam Maulana, S.Pd.)</p>
                </div>
            </div>

            <div style="page-break-before: always;">
                <div class="row">
                    <div class="col-lg-6 text-center">
                        <div class="box" style="border: 1px solid black; height: 400px; width: 250px;">
                            <div class="box-header" style="background-color: {{ ($data->jns_kelamin == 'PA') ? 'green' : 'pink' }}; color:#fff ">
                                <h4 class="box-title">PPTQ Al Azzaam</h4>
                            </div>
                            <div class="box-body">
                                <br>
                                <h5><b>KARTU PESERTA</b></h5>

                                <p>UJIAN SELEKSI SANTRI BARU</p>
                                <p>TA
                                    <?= $thnAkd . '/' . ($thnAkd + 1) ?>
                                </p>

                                <br>
                                <br>
                                <br>
                                <p class="text-center">
                                    foto 3x4
                                </p>

                                <br>
                                <br>
                                <br>
                                <br>

                                <h5><b>{{ $data->no_pendaftaran }}</b></h5>
                                <h5>{{ $data->nama }}</h5>
                            </div>
                            <div class="box-footer" style="background-color: {{ ($data->jns_kelamin == 'PA') ? 'green' : 'pink' }}; color:#fff;">
                                <h4 class="box-title" style="margin-top: 1px;">{{ $data->nm_jenjang }}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6" style="margin-left: 300px">
                        <p>
                            <b>PERHATIAN:</b> <br>
                            - Gunting kartu peserta di atas sesuai dengan garis yang terlihat <br>
                            - Harap datang saat ujian dengan membawa: <br>
                            &nbsp; 1. Undangan ujian seleksi; <br>
                            &nbsp; 2. Kartu peserta ujian seleksi; <br>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>
