<style>
  body {
    font-size: 11px;
    margin: 0;
  }

  .borderless {
    font-size: 11px;
    border-collapse: separate;
    border-spacing: 0 -15px;
    margin-top: 10px;
  }

  .borderless td,
  .borderless th {
    border: none;
  }

  .table-sm > tbody > tr > td {
    padding: 10px 10px !important;
  }

  p {
    margin-bottom: 0px;
    font-size: 11px;
  }

  .row {
    margin-bottom: 10px;
    margin-right: 40px;
  }
</style>