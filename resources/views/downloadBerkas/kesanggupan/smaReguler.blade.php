<!DOCTYPE html>
<html>

    <head>
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                font-size: 12px;
            }

            .borderless {
                font-size: 12px;
                border-collapse: separate;
                border-spacing: 0 -20px;
                margin-top: 10px;
            }

            .borderless td,
            .borderless th {
                border: none;
            }

            p {
                margin-bottom: 0px;
                font-size: 12px;
            }

            .row {
                margin-bottom: 10px;
            }
        </style>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row text-center">
                <img src="{{ url('/public/img/kop-surat-all.png') }}" alt="Kop Surat" style="width: 100%; height: 160px;">
            </div>

            <div class="row text-center">
                <div class="col-md-12">
                    <p>
                        <b>
                            SURAT PERNYATAAN PELIMPAHAN PENDIDIKAN <br>
                            DAN KESEDIAAN PEMBAYARAN DANA PENDIDIKAN CALON SANTRI <br>
                            TAHUN PELAJARAN <?= $thnAkd . '/' . ($thnAkd + 1) ?>
                        </b>
                    </p>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                    <p>
                        JENJANG : MA AL AZZAAM (REGULER)
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <i>Bismillahirrahmanirrahiim,</i>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>Kami Orang Tua/Wali Calon Santri Pesantren Tahfidzul Qur’an Al Azzaam – MA Al Azzaam,</p>
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="30%">Nama Orang Tua</td>
                                <td width="70%">: {{ $dtGeneral->nm_ayah }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Alamat</td>
                                <td width="70%">:
                                    {{ $dtGeneral->alamat . 'RT/RW' . $dtGeneral->rt . '/' . $dtGeneral->rw . ' ' . $dtGeneral->kecamatan . ' ' . $dtGeneral->kota . ' ' . $dtGeneral->provinsi }}
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">No WhatsApp Aktif</td>
                                <td width="70%">: {{ $dtGeneral->no_hp }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Orang Tua/Wali dari</td>
                                <td width="70%">: {{ $dtGeneral->nama . '  (' . $dtGeneral->no_pendaftaran . ')' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <b>A. Dengan sungguh-sungguh dan penuh kesadaran menyatakan bahwa:</b>
                        <ol>
                            <li>Menyerahkan sepenuhnya pendidikan putra/ putri kami dan menerima setiap peraturan yang
                                berlaku di PPTQ Al Azzaam – MA Al Azzaam.</li>
                            <li>
                                Apabila terjadi permasalahan selama pendidikan, maka akan diselesaikan bersama secara
                                kekeluargaan.
                            </li>
                            <li>
                                Apabila terjadi pengunduran diri pada putra/ putri kami sebelum Tahun Ajaran baru
                                dimulai, maka dapat mengikuti
                                mekanisme pada poin C, D dan E (halaman selanjutnya)
                            </li>
                        </ol>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <b>
                            B. Dengan ikhlas menginfaqkan Biaya Daftar Ulang kepada Pesantren Tahfidzul Qur’an Al Azzaam
                            - MA Al Azzaam Tahun Ajaran <?= $thnAkd . '/' . ($thnAkd + 1) ?>,
                            dengan rincian sebagai berikut:
                        </b>
                    </p>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Keterangan</th>
                                <th>Putra</th>
                                <th>Putri</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    1. SPP 1 Bulan Pertaman
                                </td>
                                <td>Rp. 1.350.000</td>
                                <td>Rp. 1.350.000</td>
                            </tr>
                            <tr>
                                <td>
                                    2. Paket Pendidikan 1 tahun dan Seragam
                                </td>
                                <td>Rp. 3.800.000</td>
                                <td>Rp. 4.100.000</td>
                            </tr>
                            <tr>
                                <td>
                                    3. Wakaf Gedung dan Sarpras
                                </td>
                                <td>Rp. 8.500.000</td>
                                <td>Rp. 8.500.000</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><b>Jumlah Total</b></td>
                                <td><b>Rp. 13.650.000</b></td>
                                <td><b>Rp. 13.950.000</b></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <b>
                            C. Bersedia menyelesaikan pelunasan pembayaran Biaya Daftar Ulang sebesar 100% pada tanggal
                            14-19 Februari 2022.
                        </b>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <b>
                            D. Bagi orang tua/ wali santri yang mengundurkan diri setelah daftar ulang maka uang yang
                            sudah dibayarkan tidak bisa
                            ditarik kembali ketika santri sudah masuk pesantren (tanggal 7 Juli 2022).
                        </b>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <b>
                            E. Bagi orang tua/ wali santri yang menundurkan diri setelah membayar daftar ulang, maka
                            uang yang sudah dibayarkan bisa di
                            kembalikan apabila pengunduran dirinya dilakukan sebelum santri masuk pesatren (7 Juli 2022)
                            dengan rincian :
                        </b>
                        <ol>
                            <li>Biaya paket pendidikan (Rp.3.000.000)</li>
                            <li>Seragam/bahan</li>
                            <li>SPP bulan juli 2022 (Rp.1.350.000)</li>
                        </ol>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <b>
                            F. Pilihan kesanggupan biaya SPP MA AL AZZAAM, (Siklus per 1 Tahun)
                        </b>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Pilihan Golongan SPP</th>
                                    <th class="text-right">Nominal</th>
                                    <th class="text-center">Centang pilihan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Golongan 1</td>
                                    <td class="text-right">Rp. 1.350.000</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Golongan 2</td>
                                    <td class="text-right">Rp. 1.500.000</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Golongan 3</td>
                                    <td class="text-right">Rp. 1.750.000</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Golongan 4 (Lebih dari Rp. 1.750.000)</td>
                                    <td class="text-right">Rp. </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        Demikian pernyataan ini kami buat, semoga memberikan manfaat yang sebesar–besarnya pada
                        pendidikan putra/ putri kami dan
                        mendapatkan ridho Allah Subhanahu Wa Ta’ala. Aamiin.
                    </p>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    <p>.............................., {{ date('Y') }}</p>
                    <p>Yang membuat pernyataan,</p>

                    <br>
                    <p style="color: grey">Materai 10.000</p>
                    <br>
                    <p>(...............................)</p>
                </div>
            </div>

        </div>

    </body>

</html>
