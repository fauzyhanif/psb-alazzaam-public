<!DOCTYPE html>
<html>

    <head>
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('downloadBerkas.css');
        
    </head>

    <body>
        <div class="container-fluid">
            <div class="row" style="margin-bottom: -30px;">
                <div class="col-lg-4 col-md-4">
                    <img src="{{ url('/public/img/logo_kecil.jpg') }}" alt="Kop Surat" style="width: 40%;">
                </div>
                <div class="col-lg-8 col-md-8 text-right">
                    <span style="padding-top: 15px !important"><b>FORM SP3K</b></span>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-12">
                    <p>
                        <b>
                            SURAT PERNYATAAN PELIMPAHAN PENDIDIKAN <br>
                            DAN KESEDIAAN PEMBAYARAN DANA PENDIDIKAN CALON SANTRI <br>
                            TAHUN AJARAN <?= $thnAkd . '/' . ($thnAkd + 1) ?>
                        </b>
                    </p>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <i>Bismillahirrahmanirrahiim,</i>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <p>Kami Orang Tua/Wali Calon Santri Pondok Pesantren Tahfidzul Qur’an Al Azzaam</p>
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="30%">Jenjang</td>
                                <td width="70%">: {{ $dtGeneral->nm_jenjang }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Nama Orang Tua</td>
                                <td width="70%">: {{ $dtGeneral->nm_ayah }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Alamat</td>
                                <td width="70%">:
                                    {{ $dtGeneral->alamat . 'RT/RW' . $dtGeneral->rt . '/' . $dtGeneral->rw . ' ' . $dtGeneral->kecamatan . ' ' . $dtGeneral->kota . ' ' . $dtGeneral->provinsi }}
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">No WhatsApp Aktif</td>
                                <td width="70%">: {{ $dtGeneral->no_hp }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Orang Tua/Wali dari</td>
                                <td width="70%">: {{ $dtGeneral->nama . '  (' . $dtGeneral->no_pendaftaran . ')' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {!! $konten !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 200px;">
                    <p>.............................., {{ date('Y') }}</p>
                    <p>Yang membuat pernyataan,</p>

                    <br>
                    <p style="color: grey">Materai 10.000</p>
                    <br>
                    <p>(...............................)</p>
                </div>
            </div>

        </div>

    </body>

</html>
