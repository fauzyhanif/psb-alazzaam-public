@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Dashboard PSB PPTQ Al Azzaam Gelombang {{ HelperDashboard::GelombangAktif() }} Tahun Ajaran {{ HelperDashboard::ThnAkdAktif() }}
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                <h3>{{ HelperDashboard::TotalPendaftar() }}</h3>

                    <p>Total Pendaftar</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ HelperDashboard::TotalPendaftar(1) }}</h3>

                    <p>Pendaftar SMP</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
              <a href="{{ url('/data-calon-santri/1') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <div class="inner">
                    <h3>{{ HelperDashboard::TotalPendaftar(2) }}</h3>

                    <p>Pendaftar IM</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
              <a href="{{ url('/data-calon-santri/2') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ HelperDashboard::TotalPendaftar(3) + HelperDashboard::TotalPendaftar(4) }}</h3>

                    <p>Pendaftar SMA</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
              <a href="{{ url('/data-calon-santri/3') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Rincian Pendaftar Berdasarkan Jenjang</h3>
                </div>
                <div class="box-body">
                    @foreach (HelperDataReferensi::DtJenjang() as $item)
                    @php
                        $total = 0;
                        $total =  HelperDashboard::RincianPendaftar('PI', $item->id_jenjang) +  HelperDashboard::RincianPendaftar('PA', $item->id_jenjang);
                    @endphp

                    <table class="table table-striped">
                        <thead>
                            <th colspan="2">Jenjang {{ $item->nama }}</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="50%">Putra</td>
                                <td width="50%">: {{ HelperDashboard::RincianPendaftar('PA', $item->id_jenjang) }} Pendaftar</td>
                            </tr>
                            <tr>
                                <td width="50%">Putri</td>
                                <td width="50%">: {{ HelperDashboard::RincianPendaftar('PI', $item->id_jenjang) }} Pendaftar</td>
                            </tr>
                            <tr>
                                <td width="50%"><b>Total</b></td>
                                <td width="50%">: <b>{{ $total }} Pendaftar</b></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    @endforeach

                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Pendaftar Terakhir</h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Jenjang</th>
                            <th>No. Pendaftaran</th>
                            <th>Nama</th>
                        </thead>
                        <tbody>
                            @foreach (HelperDashboard::PendaftarTerakhir() as $item)
                            <tr>
                            <td>{{ $item->jenjang }}</td>
                            <td>{{ $item->no_pendaftaran }}</td>
                            <td>{{ $item->nama }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- <h2 class="text-center">
        Selamat datang di sistem Portal PSB PPTQ Al Azzaam.
    </h2> --}}
</section>
@endsection
