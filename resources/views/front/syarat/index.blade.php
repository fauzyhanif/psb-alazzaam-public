@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Syarat Pendaftaran
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <img src="{{ url('/public/img/syarat_pendaftaran_2023.png') }}" alt="Syarat pendaftaran" style="width: 100%">
        </div>
    </div>
</section>
@endsection
