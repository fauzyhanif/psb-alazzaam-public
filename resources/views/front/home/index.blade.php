@extends('index')


@section('content')
@include('front.home.css')

<section class="content">
    @if (HelperDataReferensi::ThnAkdAktif() != '0' && HelperDataReferensi::GelombangAktif() != '0')
        <div class="row">
            <div class="col-md-12">
                <div class="callout callout-info">
                    <h4>Kabar gembira! Penerimaan Santri Baru PPTQ Al Azzaam Gelombang {{ HelperDataReferensi::GelombangAktif() }} TA {{ HelperDataReferensi::ThnAkdAktif() }} <b>TELAH DIBUKA</b>.</h4>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body box-profile">
                    <div class="col-md-4">
                        <img class="profile-user-img img-responsive img-circle" src="{{ url('/public/img/logo.jpeg') }}" alt="Logo" style="width:150px;">

                        <h3 class="profile-username text-center"><b>PPTQ Al Azzaam</b></h3>

                        <p class="text-muted text-center">
                            Kp. Jetis Trawas RT 01 RW 03 Cepoko, Gunung Pati Kota Semarang KP 50229 <br>
                        </p>
                    </div>

                    <div class="col-md-8" id="kalimat-sambutan">
                        <p>
                            Assalamualaikum Warahmatullahi Wabarakatuh.
                        </p>
                        <h3>
                            <b>
                            Selamat datang di Website Pendaftaran Santri Baru PPTQ Al Azzaam, Gunungpati, Semarang.
                            </b>
                        </h3>

                        <p>
                            Sebelum mendaftar, pastikan Ananda telah membaca <a href="{{ url('/prosedur') }}">Prosedur</a> dan <a href="{{ url('/syarat') }}">Syarat</a> Pendaftaran terlebih dahulu.
                            Apabila sudah cukup jelas, silahkan klik FORMULIR PENDAFTARAN Putra untuk Calon
                            Santri dan klik FORMULIR PENDAFTARAN Putri untuk Calon Santriwati pada box dibawah ini.

                            <br>
                            <br>
                            Jika ada kebingungan atau pertanyaan silahkan hubungi Admin PSB  <br>
                            Ust. Yusuf Imam Maulana, S.Pd. <br>
                            CP : 0858-0192-4860

                            <br>
                            <br>
                            Ust. Miftahul Ulum, Lc. <br>
                            CP : 0857-7786-4046

                            <br>
                            <br>
                            Ustadzah Asma' Zahratus Shalihah <br>
                            CP : 0821-3638-5644

                            <br>
                            <br>
                            Ustadzah Feny Irminawati, S.T. <br>
                            CP : 0812-2963-3919
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (HelperDataReferensi::ThnAkdAktif() != '0' && HelperDataReferensi::GelombangAktif() != '0')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Formulir Jenjang SMP</b></h3>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-6 col-xs-6">
                            <a href="{{ url('/formulir/1/PA') }}" style="color: white">
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <p>FORMULIR PENDAFTARAN</p>
                                        <h3>PUTRA</h3>

                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <a href="{{ url('/formulir/1/PA') }}" class="small-box-footer">
                                        <b>Daftar Sekarang</b> &nbsp; <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-6 col-xs-6">
                            <a href="{{ url('/formulir/1/PI') }}" style="color: white">
                                <div class="small-box bg-maroon">
                                    <div class="inner">
                                        <p>FORMULIR PENDAFTARAN</p>
                                        <h3>PUTRI</h3>

                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <a href="{{ url('/formulir/1/PI') }}" class="small-box-footer">
                                        <b>Daftar Sekarang</b> &nbsp; <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Formulir Jenjang MA (JALUR REGULER)</b></h3>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-6 col-xs-6">
                            <a href="{{ url('/formulir/3/PA') }}" style="color: white">
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <p>FORMULIR PENDAFTARAN</p>
                                        <h3>PUTRA</h3>

                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <a href="{{ url('/formulir/3/PA') }}" class="small-box-footer">
                                        <b>Daftar Sekarang</b> &nbsp; <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-6 col-xs-6">
                            <a href="{{ url('/formulir/3/PI') }}" style="color: white">
                                <div class="small-box bg-maroon">
                                    <div class="inner">
                                        <p>FORMULIR PENDAFTARAN</p>
                                        <h3>PUTRI</h3>

                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <a href="{{ url('/formulir/3/PI') }}" class="small-box-footer">
                                        <b>Daftar Sekarang</b> &nbsp; <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Formulir Jenjang IM (I'dad Muhafidzoh)</b></h3>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-12 col-xs-12">
                            <a href="{{ url('/formulir/2/PI') }}" style="color: white">
                                <div class="small-box bg-maroon">
                                    <div class="inner">
                                        <p>FORMULIR PENDAFTARAN</p>
                                        <h3>PUTRI</h3>

                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <a href="{{ url('/formulir/2/PI') }}" class="small-box-footer">
                                        <b>Daftar Sekarang</b> &nbsp; <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>
@endsection
