<section>
    <h2 style="margin-bottom: 5px;">
        <b>F. Data Lain-lain</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group">
        <label>Penanggung Jawab Biaya <span class="text-red">*</span></label>
        <div class="radio">
            <label>
                <input type="radio" name="pj_biaya" class="required" value="Orang Tua" checked> Orang Tua
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="pj_biaya" class="required" value="Wali"> Wali
            </label>
        </div>
    </div>

    <div class="form-group">
        <label>Siapkah diuji Hafalan Qur'an 5 Juz? <span class="text-red">*</span></label>
        <div class="radio">
            <label>
                <input type="radio" name="siap_diuji_hafalan" class="required" value="Ya" checked> Ya
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="siap_diuji_hafalan" class="required" value="Tidak"> Tidak
            </label>
        </div>
    </div>
</section>
