@extends('index')

@section('content')
@include('front.formulir.css')

<section class="content-header">
    <h1>
        Formulir Pendaftaran Santri Baru PPTQ Al Azzaam TA {{ HelperDataReferensi::ThnAkdAktif() }} Gelombang {{ HelperDataReferensi::GelombangAktif() }}
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="callout callout-info">
                        <h4>Perhatian</h4>

                        <p>1. Semua pertanyaan yang bertanda bintang (*) wajib diisi.</p>
                        <p>2. Dimohon untuk menyelesaikan pengisian formulir pendaftraran sampai selesai.</p>
                        <p>
                            3. Jika ada kebingungan silahkan hubungi Admin PSB <br>
                            Ust. Yusuf Imam Maulana, S.Pd. <br>
                            CP : 0858-0192-4860

                            <br>
                            <br>
                            Ust. Miftahul Ulum, Lc. <br>
                            CP : 0857-7786-4046

                            <br>
                            <br>
                            Ustadzah Pratiwi Kurnia Widasari, S.E. <br>
                            CP : 0822-2754-2750
                        </p>
                    </div>
                </div>
                <div class="box-body">
                    <form id="example-form" action="{{ url('/formulir/daftar') }}" method="POST">

                        <input type="hidden" name="thn_akd" value="<?= substr(HelperDataReferensi::ThnAkdAktif(),0,4); ?>">
                        <input type="hidden" name="gelombang" value="<?= HelperDataReferensi::GelombangAktif(); ?>">
                        <input type="hidden" name="tes_tgl" value="<?= HelperDataReferensi::getJadwalTes()->tes_tgl; ?>">
                        <input type="hidden" name="tes_jam" value="<?= HelperDataReferensi::getJadwalTes()->tes_jam; ?>">
                        <input type="hidden" name="tes_tempat" value="<?= HelperDataReferensi::getJadwalTes()->tes_tempat; ?>">
                        <input type="hidden" name="tes_materi" value="<?= HelperDataReferensi::getJadwalTes()->tes_materi; ?>">
                        <input type="hidden" name="id_jenjang" value="{{ $datas['idJenjang'] }}">

                        <div>
                            <h3>DATA PRIBADI </h3>
                            @include('front.formulir.formGeneral')

                            <h3>DATA SEKOLAH ASAL</h3>
                            @include('front.formulir.formSekolahAsal')

                            <h3>DATA AYAH</h3>
                            @include('front.formulir.formDataAyah')

                            <h3>DATA IBU</h3>
                            @include('front.formulir.formDataIbu')

                            <h3>DATA LAIN-LAIN</h3>
                            @include('front.formulir.formDataLain')

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('front.formulir.js')
@endsection
