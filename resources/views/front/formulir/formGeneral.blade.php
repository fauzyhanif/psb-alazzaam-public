<section>
    <h2 style="margin-bottom: 5px;">
    <b>A. Data Pribadi</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group col-md-12 col-md-12">
        <label>Nama Lengkap <span class="text-red">*</span></label>
        <input type="text" name="nama" class="form-control required" placeholder="Nama Lengkap">
    </div>

    <div class="form-group col-md-12">
        <label>NISN <span class="text-red">*</span></label>
        <input type="text" name="nisn" class="form-control required" maxlength="30">
    </div>

    <div class="form-group col-md-12">
        <label>NIK <span class="text-red">*</span></label>
        <input type="text" name="nik" class="form-control required" maxlength="30">
    </div>

    <div class="form-group col-md-12">
        <label>No Kartu Keluarga (KK) <span class="text-red">*</span></label>
        <input type="text" name="kk" class="form-control required" maxlength="30">
    </div>

    <div class="form-group col-md-12">
        <label>Jenis kelamin <span class="text-red">*</span></label>
        <div class="radio">
            <label>
            <input type="radio" name="jns_kelamin" class="required" value="{{ $datas['jnsKelamin'] }}" checked> {{ $datas['jnsKelamin'] == 'PA' ? 'Laki-laki' : 'Perempuan' }}
            </label>
        </div>
    </div>

    <div class="form-group col-md-12">
        <label>Tempat lahir <span class="text-red">*</span></label>
        <input type="text" name="tmp_lahir" class="form-control required">
    </div>

    <div class="form-group col-md-4">
        <label>Tanggal Lahir <span class="text-red">*</span></label>
        <input type="text" name="tgl_lahir" class="form-control datepicker required">
    </div>

    <div class="form-group col-md-4">
        <label>Bulan Lahir <span class="text-red">*</span></label>
        <select name="bln_lahir" class="form-control required">
            <option value="">-- Pilih Bulan --</option>
            <option value="01">Januari</option>
            <option value="02">Februari</option>
            <option value="03">Maret</option>
            <option value="04">April</option>
            <option value="05">Mei</option>
            <option value="06">Juni</option>
            <option value="07">Juli</option>
            <option value="08">Agustus</option>
            <option value="09">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">Desember</option>
        </select>
    </div>

    <div class="form-group col-md-4">
        <label>Tahun Lahir <span class="text-red">*</span></label>
        <input type="text" name="thn_lahir" class="form-control yearpicker required">
    </div>

    <div class="form-group col-md-12">
        <label>Jumlah Saudara</label>
        <input type="text" name="jml_sdr" class="form-control">
    </div>

    <div class="form-group col-md-12">
        <label>Anak ke</label>
        <input type="text" name="anak_ke" class="form-control">
    </div>

    <div class="form-group col-md-12">
        <label>Golongan Darah</label>
        <select name="gol_darah" class="form-control">
            <option value="-">-- Pilih Golongan Darah --</option>
            <option>A</option>
            <option>B</option>
            <option>AB</option>
            <option>O</option>
        </select>
    </div>

    <div class="form-group col-md-12">
        <label>Alamat <span class="text-red">*</span></label>
        <textarea name="alamat" id="" class="form-control required"></textarea>
    </div>

    <div class="form-group col-md-12">
        <label>Rt <span class="text-red">*</span></label>
        <input type="number" name="rt" value="0" class="form-control required">
    </div>

    <div class="form-group col-md-12">
        <label>Rw <span class="text-red">*</span></label>
        <input type="number" name="rw" value="0" class="form-control required">
    </div>

    <div class="form-group col-md-12">
        <label>Provinsi <span class="text-red">*</span></label>
        <select name="id_provinsi"  class="form-control required" onchange="cariKota(this.value, 'GENERAL')">
            <option value="">-- Pilih Provinsi --</option>
            @foreach (HelperDataReferensi::DtProvinsi() as $data)
            <option value="{{ $data->id_wil }}">{{ $data->nm_wil }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-12">
        <label>Kota / Kabupaten <span class="text-red">*</span></label>
        <select name="id_kota"  class="form-control required" id="general-kota" onchange="cariKecamatan(this.value, 'GENERAL')">
            <option value="">-- Pilih Kota / Kabupaten --</option>
        </select>
    </div>

    <div class="form-group col-md-12">
        <label>Kecamatan <span class="text-red">*</span></label>
        <select name="id_kecamatan"  class="form-control required" id="general-kecamatan">
            <option value="">-- Pilih Kecamatan --</option>
        </select>
    </div>

    <div class="form-group col-md-12">
        <label>Kelurahan <span class="text-red">*</span></label>
        <input type="text" name="id_desa" class="form-control required">
    </div>

    <div class="form-group col-md-12">
        <label>Kode POS</label>
        <input type="text" name="kode_pos" class="form-control">
    </div>

    <div class="form-group col-md-12">
        <label>Status</label>
        <div class="radio">
            <label>
                <input type="radio" name="status_anak" class="required" value="Anak kandung" checked> Anak kandung
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="status_anak" class="required" value="Anak angkat"> Anak angkat
            </label>
        </div>
    </div>

    <div class="form-group col-md-12">
        <label>Membaca Al Qur'an</label>
        <div class="radio">
            <label>
                <input type="radio" name="membaca_alquran" class="required" value="Sudah lancar" checked> Sudah lancar
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="membaca_alquran" class="required" value="belum lancar"> belum lancar
            </label>
        </div>
    </div>

    @if ($datas['idJenjang'] == '1' || $datas['idJenjang'] == '3')
        <div class="form-group col-md-12">
            <label>Nomor Kartu Indonesia Pintar</label>
            <input type="text" name="pip" class="form-control">
        </div>
    @endif
</section>
