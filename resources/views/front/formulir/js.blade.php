<script>
$(function () {
    $('input[name="ayah_id_pendidikan"]').first().prop('checked', true)
    $('input[name="ayah_id_pekerjaan"]').first().prop('checked', true)
    $('input[name="ayah_id_penghasilan"]').first().prop('checked', true)
    $('input[name="ayah_domisili_sesuai_kk"]').first().prop('checked', true)
    $('input[name="ibu_id_pendidikan"]').first().prop('checked', true)
    $('input[name="ibu_id_pekerjaan"]').first().prop('checked', true)
    $('input[name="ibu_id_penghasilan"]').first().prop('checked', true)
    $('input[name="ibu_domisili_sesuai_kk"]').first().prop('checked', true)
    $('input[name="siap_diuji_hafalan"]').first().prop('checked', true)

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $( ".datepicker" ).datepicker({
        format: "dd",
        maxViewMode: 0,
        autoclose: true
    });

    $( ".monthpicker" ).datepicker({
        format: "mm",
        viewMode: "months",
        minViewMode: "months",
        autoclose: true
    });

    $( ".yearpicker" ).datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

});

var form = $("#example-form");
form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        confirm: {
            equalTo: "#password"
        }
    }
});
form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
        var url         = form.prop('action');
        $.ajax({
            type: 'POST',
            url: url,
            dataType: "JSON",
            data: form.serialize(),
            beforeSend: function() {
                var htmlLoading = "<br><br><br><br><br><br><br><center><h1>Mohon tunggu, Proses Pendaftaran sedang berlangsung</h1></center>";
                $('body').html(htmlLoading);
            },
            success: function(result){
                var base = {!! json_encode(url('/pendaftaran-berhasil')) !!};
                var urlReload = base + "/" + result;

                //this will redirect us in same window
                document.location.href = urlReload;
            }
        });
    }
});

function cariKota(IdProvinsi, ID) {
    var base = {!! json_encode(url('/helpers/cari-kota')) !!};
    var url = base + "/" + IdProvinsi;
    $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
            var y = '<option value="">-- Pili Kota/Kabupaten --</option>'
            for (var x in result){
                y += '<option value="'+result[x].id_wil+'">'+result[x].nm_wil+'</option>'
            }

            if (ID === 'GENERAL') {
                $('#general-kota').html(y)
            } else if(ID === 'SEKOLAH') {
                $('#sekolah-kota').html(y)
            }

        }
    });
}

function cariKecamatan(IdKota, ID) {
    var base = {!! json_encode(url('/helpers/cari-kecamatan')) !!};
    var url = base + "/" + IdKota;
    $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
            var y = '<option value="">-- Pilih Kecamatan --</option>'
            for (var x in result){
                y += '<option value="'+result[x].id_wil+'">'+result[x].nm_wil+'</option>'
            }

            if (ID === 'GENERAL') {
                $('#general-kecamatan').html(y)
            } else if(ID === 'SEKOLAH') {
                $('#sekolah-kecamatan').html(y)
            }

        }
    });
}
</script>
