<section>
    <h2 style="margin-bottom: 5px;">
        <b>E. Data Ibu</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group col-md-12">
        <label>Nama Lengkap <span class="text-red">*</span></label>
        <input type="text" name="ibu_nama" class="form-control required">
    </div>

    <div class="form-group col-md-12">
        <label>Tempat Lahir <span class="text-red">*</span></label>
        <input type="text" name="ibu_tmp_lahir" class="form-control required">
    </div>

    <div class="form-group col-md-4">
        <label>Tanggal Lahir <span class="text-red">*</span></label>
        <input type="text" name="ibu_tgl_lahir" class="form-control datepicker required">
    </div>

    <div class="form-group col-md-4">
        <label>Bulan Lahir <span class="text-red">*</span></label>
        <select name="ibu_bln_lahir" class="form-control required">
            <option value="">-- Pilih Bulan --</option>
            <option value="01">Januari</option>
            <option value="02">Februari</option>
            <option value="03">Maret</option>
            <option value="04">April</option>
            <option value="05">Mei</option>
            <option value="06">Juni</option>
            <option value="07">Juli</option>
            <option value="08">Agustus</option>
            <option value="09">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">Desember</option>
        </select>
    </div>

    <div class="form-group col-md-4">
        <label>Tahun Lahir <span class="text-red">*</span></label>
        <input type="text" name="ibu_thn_lahir" class="form-control yearpicker required">
    </div>

    <div class="form-group col-md-12">
        <label>No HP / WA <span class="text-red">*</span></label>
        <input type="text" name="ibu_no_hp" class="form-control required" maxlength="20">
    </div>

    <div class="form-group col-md-12">
        <label>Status <span class="text-red">*</span></label>
        <div class="radio">
            <label>
                <input type="radio" name="ibu_status_hidup" class="required" value="Masih Hidup" checked> Masih Hidup
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="ibu_status_hidup" class="required" value="Wafat"> Wafat
            </label>
        </div>
    </div>

    <div class="form-group col-md-12">
        <label>NIK <span class="text-red">*</span></label>
        <input type="text" name="ibu_nik" class="form-control" maxlength="30">
    </div>

    <div class="form-group col-md-12">
        <label>Pendidikan Terakhir <span class="text-red">*</span></label>
        <select name="ibu_id_pendidikan" class="form-control required">
            <option value="">-- Pilih Pendidikan --</option>
            @foreach (HelperDataReferensi::DtPendidikan() as $data)
                <option value="{{ $data->id }}">{{ $data->nama }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-12">
        <label>Pekerjaan <span class="text-red">*</span></label>
        <select name="ibu_id_pekerjaan" class="form-control required">
            <option value="">-- Pilih Pendidikan --</option>
            @foreach (HelperDataReferensi::DtPekerjaan() as $data)
                <option value="{{ $data->id }}">{{ $data->nama }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-12">
        <label>Domisili sesuai KK?</label>
        <div class="radio">
            <label>
                <input type="radio" name="ibu_domisili_sesuai_kk" class="required checked" value="Ya" checked> Ya
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="ibu_domisili_sesuai_kk" class="required" value="Tidak"> Tidak
            </label>
        </div>
    </div>
</section>
