<section>
    <h2 style="margin-bottom: 5px;">
        <b>C. Jenjang Tujuan</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group">
        <label>Jenjang Tujuan <span class="text-red">*</span></label>
        <div class="radio">
            @foreach (HelperDataReferensi::DtJenjang() as $data)
                <label>
                    <input type="radio" name="id_jenjang" class="required" value="{{ $data->id_jenjang }}" @if($datas['idJenjang'] == $data->id_jenjang ) checked @endif> {{ $data->nama }}
                </label>
                &nbsp;
                &nbsp;
            @endforeach
        </div>
    </div>

    @if ($datas['idJenjang'] == 3)
        <div class="form-group">
            <label>Jalur <span class="text-red">*</span></label>
            <div class="radio">
                <label>
                    <input type="radio" name="jalur" class="required" value="REGULER" checked> REGULER
                </label>
                &nbsp;
                &nbsp;
                <label>
                    <input type="radio" name="jalur" class="required" value="KHUSUS"> KHUSUS
                </label>
            </div>
        </div>
    @endif
</section>
