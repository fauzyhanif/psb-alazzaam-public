<style>
.datepicker-days>table>thead>tr>th.prev,
.datepicker-days>table>thead>tr>th.datepicker-switch,
.datepicker-days>table>thead>tr>th.next {
  display: none;
}

/* Hide days of previous month */
td.old.day{
  visibility: hidden;
}

/* Hide days of next month */
td.new.day{
  display: none;
}

.full-height {
    height: 100vh;
}

.flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
}

.position-ref {
    position: relative;
}

.code {
    border-right: 2px solid;
    font-size: 26px;
    padding: 0 15px 0 15px;
    text-align: center;
}

@media (max-width: 600px)
{
    .wizard > .steps > ul > li
    {
        width: 50%;
    }

    .wizard > .steps a,
    .wizard > .steps a:hover,
    .wizard > .steps a:active
    {
        margin-top: 0.5em;
    }

    .wizard.vertical > .steps,
    .wizard.vertical > .actions
    {
        display: block;
        float: none;
        width: 100%;
    }

    .wizard.vertical > .content
    {
        display: block;
        float: none;
        margin: 0 0.5em 0.5em;
        width: auto;
    }
}

@media (max-width: 480px)
{
    .wizard > .steps > ul > li
    {
        width: 100%;
    }
}
</style>
