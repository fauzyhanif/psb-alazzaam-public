@extends('index')

@section('content')

<section class="content-header">
    <h1>
        Prosedur Pendaftaran
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="{{ url('/public/img/ALUR PSB_2023-2024-1.png') }}" alt="Brosur Depan" style="width: 100%">
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="{{ url('/public/img/ALUR PSB_2023-2024-2.png') }}" alt="Brosur Depan" style="width: 100%">
        </div>
    </div>
</section>

@endsection
