@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Informasi PSB PPTQ Alazzaam
        <a href="{{ url('/informasi-psb/download-brosur') }}" class="btn btn-success" target="_blank">
            <i class="fa fa-download"></i>
            Download Brosur
        </a>
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <img src="{{ url('/public/img/brosur_psb_2024_1_depan.jpg') }}" alt="Brosur Depan" style="width: 100%">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <img src="{{ url('/public/img/brosur_psb_2024_1_belakang.jpg') }}" alt="Brosur Belakang" style="width: 100%">
        </div>
    </div>
</section>
@endsection
