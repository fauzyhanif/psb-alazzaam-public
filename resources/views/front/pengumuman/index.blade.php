@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Pengumuman
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <img class="card-img-top" src="{{ ('img/card.svg') }}" alt="Card image cap" style="height: 180px; width: 100%; display: block;">
                <div class="card-body">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="{{ url('/detail-pengumuman') }}">Baca Selengkapnya ></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <img class="card-img-top" src="{{ ('img/card.svg') }}" alt="Card image cap" style="height: 180px; width: 100%; display: block;">
                <div class="card-body">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="{{ url('/detail-pengumuman') }}">Baca Selengkapnya ></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <img class="card-img-top" src="{{ ('img/card.svg') }}" alt="Card image cap" style="height: 180px; width: 100%; display: block;">
                <div class="card-body">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="{{ url('/detail-pengumuman') }}">Baca Selengkapnya ></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <img class="card-img-top" src="{{ ('img/card.svg') }}" alt="Card image cap" style="height: 180px; width: 100%; display: block;">
                <div class="card-body">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="{{ url('/detail-pengumuman') }}">Baca Selengkapnya ></a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
