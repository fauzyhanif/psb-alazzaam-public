<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">

        <style type="text/css">
            body {
                font-family: 'Open Sans', sans-serif;
            }
        </style>

        <?php
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=data_calon_santri.xls");
        ?>

    </head>

    <body>
        <table>
            <tbody>
                <tr>
                    <th>Jenjang</th>
                    <th>No Pendaftaran</th>
                    <th>Nama Lengkap</th>
                    <th>Username (login)</th>
                    <th>Password (Login)</th>
                    <th>NISN</th>
                    <th>NIK</th>
                    <th>Jenis Kelamin</th>
                    <th>TTL</th>
                    <th>Jumlah Hafalan</th>
                    <th>Anak Ke</th>
                    <th>Golongan Darah</th>
                    <th>PJ Biaya</th>
                    <th>Alamat</th>
                    <th>RT/RW</th>
                    <th>Desa</th>
                    <th>Kecamatan</th>
                    <th>Kota</th>
                    <th>Provinsi</th>
                    <th>Kode Pos</th>
                    <th>Hoby</th>
                    <th>Kartu Peserta PIP</th>
                    <th>Asal Sekolah</th>
                    <th>NSPN</th>
                    <th>Tahun Lulus</th>
                    <th>Alamat Sekolah</th>
                    <th>Desa Sekolah</th>
                    <th>Kecamatan Sekolah</th>
                    <th>Kota Sekolah</th>
                    <th>Provinsi Sekolah</th>
                    <th>Kode Pos Sekolah</th>
                    <th>Nama Ayah</th>
                    <th>No Hp Ayah</th>
                    <th>Status Hidup Ayah</th>
                    <th>NIK Ayah</th>
                    <th>TTL Ayah</th>
                    <th>Pendidikan Terakhir Ayah</th>
                    <th>Pekerjaan Ayah</th>
                    <th>Penghasilan Ayah</th>
                    <th>Nama Ibu</th>
                    <th>No Hp Ibu</th>
                    <th>Status Hidup Ibu</th>
                    <th>NIK Ibu</th>
                    <th>TTL Ibu</th>
                    <th>Pendidikan Terakhir Ibu</th>
                    <th>Pekerjaan Ibu</th>
                    <th>Penghasilan Ibu</th>
                </tr>
                @for ($i = 0; $i < count($datas); $i++)
                    <tr>
                        <td>{{ $datas[$i]->nm_jenjang }}</td>
                        <td>{{ $datas[$i]->no_pendaftaran }}</td>
                        <td>{{ $datas[$i]->nama }}</td>
                        <td>{{ $datas[$i]->no_pendaftaran }}</td>
                        <td>{{ $datas[$i]->password }}</td>
                        <td>{{ (string) $datas[$i]->nisn }}</td>
                        <td>{{ (string) $datas[$i]->nik }}</td>
                        <td>{{ $datas[$i]->jns_kelamin }}</td>
                        <td>{{ $datas[$i]->tmp_lahir .", ". HelperDataReferensi::konversiTgl($datas[$i]->tgl_lahir, 'T') }}</td>
                        <td>{{ $datas[$i]->jml_hafalan }}</td>
                        <td>{{ $datas[$i]->anak_ke . " dari " . $datas[$i]->jml_sdr }}</td>
                        <td>{{ $datas[$i]->gol_darah }}</td>
                        <td>{{ $datas[$i]->pj_biaya }}</td>
                        <td>{{ $datas[$i]->alamat }}</td>
                        <td>{{ $datas[$i]->rt . "/" . $datas[$i]->rw }}</td>
                        <td>{{ $datas[$i]->id_desa }}</td>
                        <td>{{ $datas[$i]->csn_nm_kec }}</td>
                        <td>{{ $datas[$i]->csn_nm_kab }}</td>
                        <td>{{ $datas[$i]->csn_nm_prov }}</td>
                        <td>{{ $datas[$i]->kode_pos }}</td>
                        <td>{{ $datas[$i]->hoby }}</td>
                        <td>{{ $datas[$i]->pip }}</td>
                        <td>{{ $datas[$i]->sklh_nm }}</td>
                        <td>{{ $datas[$i]->sklh_nspn }}</td>
                        <td>{{ $datas[$i]->sklh_thn_lulus }}</td>
                        <td>{{ $datas[$i]->sklh_alamat }}</td>
                        <td>{{ $datas[$i]->sklh_desa }}</td>
                        <td>{{ $datas[$i]->sklh_nm_kec }}</td>
                        <td>{{ $datas[$i]->sklh_nm_kab }}</td>
                        <td>{{ $datas[$i]->sklh_nm_prov }}</td>
                        <td>{{ $datas[$i]->sklh_kode_pos }}</td>
                        <td>{{ $datas[$i]->ayah_nm }}</td>
                        <td>{{ $datas[$i]->ayah_no_hp }}</td>
                        <td>{{ $datas[$i]->ayah_status_hidup }}</td>
                        <td>{{ (string) $datas[$i]->ayah_nik }}</td>
                        <td>{{ $datas[$i]->ayah_tmp_lahir .", ". HelperDataReferensi::konversiTgl($datas[$i]->ayah_tgl_lahir, 'T') }}</td>
                        <td>{{ $datas[$i]->ayah_nm_pendidikan }}</td>
                        <td>{{ $datas[$i]->ayah_nm_pekerjaan }}</td>
                        <td>{{ $datas[$i]->ayah_nm_penghasilan }}</td>
                        <td>{{ $datas[$i]->ibu_nm }}</td>
                        <td>{{ $datas[$i]->ibu_no_hp }}</td>
                        <td>{{ $datas[$i]->ibu_status_hidup }}</td>
                        <td>{{ (string) $datas[$i]->ibu_nik }}</td>
                        <td>{{ $datas[$i]->ibu_tmp_lahir .", ". HelperDataReferensi::konversiTgl($datas[$i]->ibu_tgl_lahir, 'T') }}</td>
                        <td>{{ $datas[$i]->ibu_nm_pendidikan }}</td>
                        <td>{{ $datas[$i]->ibu_nm_pekerjaan }}</td>
                        <td>{{ $datas[$i]->ibu_nm_penghasilan }}</td>
                    </tr>
                @endfor
            </tbody>
        </table>
    </body>
</html>
