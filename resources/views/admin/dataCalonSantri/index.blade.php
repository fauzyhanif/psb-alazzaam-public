@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Data Calon Santri TA {{ Session::get('thn_akd') . '/' . (Session::get('thn_akd') + 1) }} Gelombang {{ Session::get('gelombang') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> PSB</a></li>
        <li class="active">Data Calon Santri</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <input type="hidden" name="jenjang" value="{{ $jenjang }}">
                        <select name="id_jenjang" class="form-control" style="width: 200px;" onchange="switchJenjang(this.value)">
                            <option value="*">-- Semua Jenjang --</option>
                            @foreach ($dtJenjang as $data)
                                <option value="{{ $data->id_jenjang }}" {{ $jenjang == $data->id_jenjang ? 'selected' : '' }}>{{ $data->nama }}</option>
                            @endforeach
                        </select>
                    </h3>

                    <div class="box-tools">
                        <a href="{{ url('/data-calon-santri-export') }}" target="_blank" class="btn btn-success">
                            EXPORT EXCEL
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-responsive" id="data-table">
                        <thead>
                            <th>Jenjang</th>
                            <th>No. Pendaftaran</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>TTL</th>
                            <th>No. Handphone</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    var jenjang = $('input[name="jenjang"]').val();

    if (jenjang == '*') {
        var base = {!! json_encode(url('/json-data-calon-santri')) !!};
    } else {
        var base = {!! json_encode(url('/json-data-calon-santri', $jenjang)) !!};
    }

    $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'jenjang', name: 'jenjang', searchable: false },
            { data: 'no_pendaftaran', name: 'a.no_pendaftaran', searchable: true },
            { data: 'nama', name: 'a.nama', searchable: true },
            { data: 'jns_kelamin', name: 'jns_kelamin', searchable: false },
            { data: 'tgl_lahir', name: 'tgl_lahir', searchable: false },
            { data: 'no_hp', name: 'f.no_hp', searchable: false },
            { data: 'kota', name: 'kota', searchable: false },
            { data: 'link', name: 'link' },
        ]
    });
});

function switchJenjang(idJenjang) {
    var base = {!! json_encode(url('/data-calon-santri')) !!};
    var urlReload = base + "/" + idJenjang;

    //this will redirect us in same window
    document.location.href = urlReload;
}
</script>
@endsection
