@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <h1>
            <a href="{{ url('/data-calon-santri') }}" class="btn btn-default">
                <i class="fa fa-long-arrow-left"></i> Kembali
            </a>
        </h1>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> PSB</a></li>
        <li class="active">Data Calon Santri</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Bukti Pembayaran {{ $dtGeneral->nama }} ({{ $dtGeneral->no_pendaftaran }})
                    </h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Nominal Tagihan</th>
                            <th>File</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    @currency($dtGeneral->biaya)
                                </td>
                                <td>
                                    <a href="{{ url('/dokumen-pendaftar', $dtGeneral->file_bukti_pembayaran) }}" target="_blank">{{ $dtGeneral->file_bukti_pembayaran }}</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Form Validasi
                    </h3>
                </div>
                <form action="{{ url('/admin/do-validasi-pembayaran', $dtGeneral->no_pendaftaran) }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label>Status Validasi</label>
                            <select name="konfirmasi_bukti_pembayaran" class="form-control">
                                <option value="0" {{ $dtGeneral->konfirmasi_bukti_pembayaran == 0 ? 'selected' : '' }}>Menunggu</option>
                                <option value="1" {{ $dtGeneral->konfirmasi_bukti_pembayaran == 1 ? 'selected' : '' }}>Diterima</option>
                                <option value="2" {{ $dtGeneral->konfirmasi_bukti_pembayaran == 2 ? 'selected' : '' }}>Ditolak</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-primary">
                            Simpan Validasi
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
