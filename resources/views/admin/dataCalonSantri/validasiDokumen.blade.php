@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <a href="{{ url('/data-calon-santri') }}" class="btn btn-default">
            <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> PSB</a></li>
        <li class="active">Data Calon Santri</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Dokumen {{ $dtGeneral->nama }} ({{ $dtGeneral->no_pendaftaran }})
                    </h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Nama Dokumen</th>
                            <th>File</th>
                        </thead>
                        <tbody>
                            @foreach ($dtRefDokumen as $item)
                            <tr>
                                <td>{{ $item->nama }}</td>
                                <td>
                                    @if (array_key_exists($item->id, $arrDokumenUploaded))
                                        <a href="{{ url('dokumen-pendaftar', $arrDokumenUploaded[$item->id]) }}"
                                        target="_blank">{{ $arrDokumenUploaded[$item->id] }}</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Form Validasi
                    </h3>
                </div>
                <form action="{{ url('/admin/do-validasi-dokumen', $dtGeneral->no_pendaftaran) }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label>Status Validasi</label>
                            <select name="status_upload_dokumen" class="form-control">
                                <option value="0" {{ $dtGeneral->status_upload_dokumen == 0 ? 'selected' : '' }}>Menunggu</option>
                                <option value="1" {{ $dtGeneral->status_upload_dokumen == 1 ? 'selected' : '' }}>Diterima</option>
                                <option value="2" {{ $dtGeneral->status_upload_dokumen == 2 ? 'selected' : '' }}>Direvisi</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Catatan</label>
                            <textarea name="catatan_upload_dokumen" class="form-control">{{ $dtGeneral->catatan_upload_dokumen }}</textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-primary">
                            Simpan Validasi
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
