@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <h1>
            <a href="{{ url('/data-calon-santri') }}" class="btn btn-default">
                <i class="fa fa-long-arrow-left"></i> Kembali
            </a>
        </h1>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> PSB</a></li>
        <li class="active">Data Calon Santri</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Form Setting Undangan
                    </h3>
                </div>
                <form action="{{ url('/admin/do-set-undangan', $dtGeneral->no_pendaftaran) }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name="tes_tgl" class="form-control datepicker" value="{{ $dtGeneral->tes_tgl }}">
                        </div>
                        <div class="form-group">
                            <label>Pukul</label>
                            <input type="text" name="tes_jam" class="form-control" placeholder="Ex: 09.00" value="{{ $dtGeneral->tes_jam }}">
                        </div>
                        <div class="form-group">
                            <label>Tempat</label>
                            <textarea name="tes_tempat" class="form-control">{{ $dtGeneral->tes_tempat }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Materi <span class="text-blue">(jika ada beberapa item maka pisahkan dengan koma)</span></label>
                            <textarea name="tes_materi" class="form-control">{{ $dtGeneral->tes_materi }}</textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-primary">
                            Simpan Undangan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
$(function () {
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        defaultDate : new Date()
    }).datepicker('setDate', new Date());
})
</script>
@endsection
