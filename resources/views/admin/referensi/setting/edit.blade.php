@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <a href="{{ url('/setting') }}" class="btn btn-default">
            <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li>Setting</li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Edit Data
                    </h3>
                </div>
                <form action="{{ url('/setting/edit', $data->id) }}" method="POST">
                    <div class="box-body">
                        @csrf
                        <div class="form-group">
                            <label>Parameter</label>
                            <input type="text" class="form-control" name="parameter" value="{{ $data->parameter }}" required>
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="description" required class="form-control">{{ $data->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Value</label>
                            <textarea name="value" id="editor1" required class="form-control">{{ $data->value }}</textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        CKEDITOR.replace('editor1')
    })
    </script>
@endsection

