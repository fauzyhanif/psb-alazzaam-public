@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Manajemen Usergroup
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li class="active">Usergroup</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title col-md-6">
                        List Data
                    </h3>
                    <div class="col-md-6">
                    <a href="{{ url('/usergroup/form-add') }}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus"></i> Tambah Usergroup
                        </a>
                    </div>
                </div>
                <div class="box-body">

                    <table class="table table-striped table-responsive">
                        <thead>
                            <th width="20%">ID Usergroup</th>
                            <th>Nama Usergroup</th>
                            <th width="15%">Aksi</th>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data)
                            <tr>
                                <td>#{{ $data->id_usergroup }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>
                                    <a href="{{ url('usergroup/form-edit', $data->id_usergroup) }}" class="btn btn-default btn-xs">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                    <a href="{{ url('usergroup/delete', $data->id_usergroup) }}" class="btn btn-default btn-xs">
                                        <i class="fa fa-trash"></i> Hapus
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
