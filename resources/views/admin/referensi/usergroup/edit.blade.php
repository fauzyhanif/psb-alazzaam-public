@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <a href="{{ url('/usergroup') }}" class="btn btn-default">
            <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li>Usergroup</li>
        <li class="active">Add</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Edit Data
                    </h3>
                </div>
                <form action="{{ url('/usergroup/edit', $data->id_usergroup) }}" method="POST">
                    <div class="box-body">
                        @csrf
                        <div class="form-group">
                            <label>ID Usergroup</label>
                        <input type="number" class="form-control" name="id_usergroup" value="{{ $data->id_usergroup }}">
                        </div>

                        <div class="form-group">
                            <label>Usergroup</label>
                            <input type="text" class="form-control" name="nama" value="{{ $data->nama }}">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
