@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <a href="{{ url('/sesi') }}" class="btn btn-default">
            <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li>Sesi</li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>
        <div class="col-md-6 col-md-offset-3">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Edit Data
                    </h3>
                </div>
                <form action="{{ url('/sesi/edit', $data->id) }}" method="POST">
                    <div class="box-body">
                        @csrf
                        <div class="form-group">
                            <label>Tahun Akademik</label>
                            <input type="text" class="form-control" name="thn_akd" value="{{ $data->thn_akd }}">
                        </div>

                        <div class="form-group">
                            <label>Gelombang</label>
                            <input type="text" class="form-control" name="gelombang" value="{{ $data->gelombang }}">
                        </div>

                        <div class="form-group">
                            <label>Tanggal mulai</label>
                            <input type="date" name="tgl_mulai" class="form-control" value="{{ $data->tgl_mulai }}">
                        </div>

                        <div class="form-group">
                            <label>Tanggal selesai</label>
                            <input type="date" name="tgl_selesai" class="form-control" value="{{ $data->tgl_selesai }}">
                        </div>

                        <div class="form-group">
                            <label>Biaya Pendaftaran</label>
                            <input type="text" name="biaya" class="form-control money" value="{{ $data->biaya }}">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Tes</label>
                            <input type="date" name="tes_tgl" class="form-control" value="{{ $data->tes_tgl }}">
                        </div>

                        <div class="form-group">
                            <label>Jam Tes</label>
                            <input type="text" name="tes_jam" class="form-control" placeholder="Ex: 09.00" value="{{ $data->tes_jam }}">
                        </div>

                        <div class="form-group">
                            <label>Tempat Tes</label>
                            <textarea name="tes_tempat" class="form-control">{{ $data->tes_tempat }}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Materi Tes <span class="text-blue">(jika ada beberapa item maka pisahkan dengan koma)</span></label>
                            <textarea name="tes_materi" class="form-control">{{ $data->tes_materi }}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Is Aktif?</label>
                            <select name="is_aktif" class="form-control">
                                <option value="Y" {{ $data->is_aktif == 'Y' ? 'selected' : '' }}>Aktif</option>
                                <option value="N" {{ $data->is_aktif == 'N' ? 'selected' : '' }}>Non Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

