@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Manajemen Sesi
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li class="active">Sesi</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title col-md-6">
                        List Data
                    </h3>
                    <div class="col-md-6">
                    <a href="{{ url('/sesi/form-add') }}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus"></i> Tambah Sesi
                        </a>
                    </div>
                </div>
                <div class="box-body">

                    <table class="table table-striped table-responsive">
                        <thead>
                            <th>Tahun Akademik</th>
                            <th>Gelombang</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Aktif</th>
                            <th width="15%">Aksi</th>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data)
                            <tr>
                                <td>{{ $data->thn_akd }}</td>
                                <td>{{ $data->gelombang }}</td>
                                <td>{{ $data->tgl_mulai }}</td>
                                <td>{{ $data->tgl_selesai }}</td>
                                <td>
                                    @if($data->is_aktif == 'Y')
                                        <small class="label label-success">Sedang berlangsung</small>
                                    @else
                                        <small class="label label-danger">Non Aktif</small>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('/sesi/form-edit', $data->id) }}" class="btn btn-default btn-xs">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                    <a href="{{ url('/sesi/delete', $data->id) }}" class="btn btn-default btn-xs">
                                        <i class="fa fa-trash"></i> Hapus
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
