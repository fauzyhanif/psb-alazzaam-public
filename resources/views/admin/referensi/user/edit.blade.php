@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <a href="{{ url('/user') }}" class="btn btn-default">
            <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li>User</li>
        <li class="active">Add</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Tambah Data
                    </h3>
                </div>
                <form action="{{ url('/user/edit', $data->id) }}" method="POST">
                    <div class="box-body">
                        @csrf
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" name="nama" class="form-control" value="{{ $data->nama }}">
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" value="{{ $data->nama }}">
                        </div>
                        <div class="form-group">
                            <label>Password <span class="text-blue"><i>(Biarkan kosong jika tidak ingin ubah password)</i></span></label>
                            <input type="text" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Usergroup</label>
                            <select name="id_usergroup[]" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup" style="width: 100%;">
                                <?php
                                    $r = array_filter(explode(',', $data->id_usergroup));
                                    foreach ($dt_usergroup as $x){
                                        if (in_array($x->id_usergroup, $r)){
                                            echo "<option value='$x->id_usergroup' selected>$x->nama</option>";
                                        } else {
                                            echo "<option value='$x->id_usergroup'>$x->nama</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenjang Pendidikan</label>
                            <select name="id_jenjang[]" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup" style="width: 100%;">
                                <?php
                                    $r = array_filter(explode(',', $data->id_jenjang));
                                    foreach ($dt_jenjang as $x){
                                        if (in_array($x->id_jenjang, $r)){
                                            echo "<option value='$x->id_jenjang' selected>$x->nama</option>";
                                        } else {
                                            echo "<option value='$x->id_jenjang'>$x->nama</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
