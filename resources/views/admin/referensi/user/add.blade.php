@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <a href="{{ url('/user') }}" class="btn btn-default">
            <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li>User</li>
        <li class="active">Add</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Tambah Data
                    </h3>
                </div>
                <form action="{{ url('/user/add-new') }}" method="POST">
                    <div class="box-body">
                        @csrf
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" name="password" value="{{ old('password') }}" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label>Usergroup</label>
                            <select name="id_usergroup[]" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup" style="width: 100%;">
                                @foreach ($dt_usergroup as $x)
                                    <option value="{{ $x->id_usergroup }}">{{ $x->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenjang Pendidikan</label>
                            <select name="id_jenjang[]" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup" style="width: 100%;">
                                @foreach ($dt_jenjang as $x)
                                    <option value="{{ $x->id_jenjang }}">{{ $x->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
