@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Manajemen User
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li class="active">User</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title col-md-6">
                        List Data
                    </h3>
                    <div class="col-md-6">
                    <a href="{{ url('/user/form-add') }}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus"></i> Tambah User
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-responsive" id="users-table">
                        <thead>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Usergroup</th>
                            <th width="30%">Aksi</th>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'user-json',
        columns: [
            { data: 'nama', name: 'nama' },
            { data: 'username', name: 'username' },
            { data: 'jenis', name: 'jenis' },
            { data: 'aksi', name: 'aksi' }
        ]
    });

});
</script>

@endsection
