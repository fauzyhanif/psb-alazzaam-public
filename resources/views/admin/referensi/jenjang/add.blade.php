@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <a href="{{ url('/jenjang') }}" class="btn btn-default">
            <i class="fa fa-long-arrow-left"></i> Kembali
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> Referensi</a></li>
        <li>Jenjang Pendidikan</li>
        <li class="active">Add</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Tambah Data
                    </h3>
                </div>
                <form action="{{ url('/jenjang/add-new') }}" method="POST">
                    <div class="box-body">
                        @csrf
                        <div class="form-group">
                            <label>ID Jenjang</label>
                            <input type="text" name="id_jenjang" class="form-control" placeholder="ID Jenjang">
                        </div>
                        <div class="form-group">
                            <label>Nama Jenjang Pendidikan</label>
                            <input type="text" name="nama" class="form-control" placeholder="Jenjang Pendidikan">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
