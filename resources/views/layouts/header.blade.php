<?php use Illuminate\Support\Facades\Session; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PSB SMP Bina Insani</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ url('/public/assets/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Datepicker -->
    <link rel="stylesheet" href="{{ url('/public/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    {{-- datatable --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/public/assets/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ url('/public/assets/Ionicons/css/ionicons.min.css') }}">
    <!-- Select -->
    <link rel="stylesheet" href="{{ url('/public/assets/select2/dist/css/select2.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('/public/admin-lte/css/AdminLTE.min.css') }}">
    <!-- Steps style -->
    <link rel="stylesheet" href="{{ url('/public/assets/jquery.steps-1.1.0/jquery.steps.new.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ url('/public/admin-lte/css/skins/_all-skins.min.css') }}">

    <!-- jQuery 3 -->
    <script src="{{ url('/public/assets/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ url('/public/assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <!-- jQuery Steps -->
    <script src="{{ url('/public/assets/jquery.steps-1.1.0/jquery.steps.min.js') }}"></script>
    <!-- Select -->
    <script src="{{ url('/public/assets/select2/dist/js/select2.full.min.js') }}"></script>
    {{-- Datatable --}}
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{ url('/public/assets/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ url('/public/assets/fastclick/lib/fastclick.js') }}"></script>
    <!-- Momentjs -->
    <script src="{{ url('/public/assets/moment/min/moment.min.js') }}"></script>
    <!-- Datepicker -->
    <script src="{{ url('/public/assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Input mask -->
    <script src="{{ url('/public/assets/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('/public/assets/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('/public/assets/input-mask/jquery.inputmask.extensions.js') }}"></script>
    {{-- jsPDF --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('/public/admin-lte/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ url('/public/admin-lte/js/demo.js') }}"></script>
    {{-- ck editor --}}
    <script src="{{ url('/public/assets/ckeditor/ckeditor.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    @if(Session::get('username'))
                        @if(Session::get('usergroup_aktif') == '1' || Session::get('usergroup_aktif') == '2' || Session::get('usergroup_aktif') == '3')
                            <a href="{{ url('/dashboard') }}" class="navbar-brand"><b>PSB</b> PPTQ Al Azzaam</a>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                    <i class="fa fa-bars"></i>
                                </button>
                            </div>
                        @elseif (Session::get('usergroup_aktif') == '4')
                            <a href="#" class="navbar-brand"><b>PSB</b> PPTQ Al Azzaam</a>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                    <i class="fa fa-bars"></i>
                                </button>
                            </div>
                        @endif
                    @else
                        <a href="{{ url('/') }}" class="navbar-brand"><b>PSB</b> PPTQ Al Azzaam</a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                    @endif

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <ul class="nav navbar-nav">

                        <!-- Menu link admin -->
                        @if(Session::get('username'))
                            @if(Session::get('usergroup_aktif') == '1' || Session::get('usergroup_aktif') == '2' || Session::get('usergroup_aktif') == '3')
                                <li><a href="{{ url('/data-calon-santri') }}">Data Calon Santri</a></li>

                                @if(Session::get('usergroup_aktif') == '1' || Session::get('usergroup_aktif') == '3')
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Referensi <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ url('/user') }}">User</a></li>
                                            <li><a href="{{ url('/usergroup') }}">Usergroup</a></li>
                                            <li class="divider"></li>
                                            <li><a href="{{ url('/jenjang') }}">Jenjang Pendidikan</a></li>
                                            <li><a href="{{ url('/sesi') }}">Sesi / Gelombang</a></li>
                                            <li><a href="{{ url('/setting') }}">Setting</a></li>
                                        </ul>
                                    </li>
                                @endif

                                @if(Session::get('usergroup_aktif') != '4')
                                    <li><a href="{{ url('/pilih-sesi') }}">Ganti Sesi Tahun Akd</a></li>
                                    <li><a href="{{ url('/auth/pilih-usergroup') }}">Ganti Usergroup</a></li>
                                @endif

                            @elseif(Session::get('usergroup_aktif') == '4')
                                <!-- Menu link calon santri -->
                                <li><a href="{{ url('/calon-santri/profil') }}">Profil</a></li>
                                <li><a href="{{ url('/calon-santri/upload-dokumen') }}">Upload Dokumen</a></li>
                                <li><a href="{{ url('/calon-santri/upload-bukti-pembayaran') }}">Upload Bukti Pembayaran</a></li>
                                <li class="dropdown">
                                    <!-- Menu Toggle Button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-headphones"></i> &nbsp; Bantuan
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <!-- The user image in the menu -->
                                        <li>
                                            <a href="javascript:void(0)">Ust. Yusuf Imam Maulana, S.Pd. <br> 0858-0192-4860</a>                                        
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ust. Miftahul Ulum, Lc. <br> 0857-7786-4046</a>                                        
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ustadzah Asma' Zahratus Shahihah <br> 0821-3638-5644</a>                                        
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Ustadzah Feny Irminawati, S.T.<br> 0812-2963-3919</a>                                        
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        @else
                            <!-- Menu link front -->
                            <li><a href="{{ url('/syarat') }}">Syarat</a></li>
                            <li><a href="{{ url('/prosedur') }}">Prosedur</a></li>
                            <li><a href="{{ url('/informasi-psb') }}">Informasi PSB</a></li>
                            <li class="dropdown">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-headphones"></i> &nbsp; Bantuan
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <!-- The user image in the menu -->
                                    <li>
                                        <a href="javascript:void(0)">Ust. Yusuf Imam Maulana, S.Pd. <br> 0858-0192-4860</a>                                        
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Ust. Miftahul Ulum, Lc. <br> 0857-7786-4046</a>                                        
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Ustadzah Asma' Zahratus Shahihah <br> 0821-3638-5644</a>                                        
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Ustadzah Feny Irminawati, S.T.<br> 0812-2963-3919</a>                                        
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        @if(Session::has('username'))
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <!-- The user image in the navbar-->
                                    <img src="{{ url('/public/img/user.png') }}" class="user-image" alt="User Image">
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class="hidden-xs">{{ Session::get('nama') }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header">
                                        <img src="{{ url('/public/img/user.png') }}" class="img-circle" alt="User Image">

                                        <p>
                                            {{ Session::get('nama') }} <br>
                                            @if(Session::get('usergroup_aktif') == '4')
                                                {{ Session::get('username') }}
                                            @endif
                                        <small>
                                            @php if (Session::has('usergroup_aktif')) {
                                                echo Session::get('nama_usergroup_aktif');
                                            } @endphp
                                        </small>
                                        </p>
                                    </li>
                                <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <a href="{{ url('/logout') }}" class="btn btn-default btn-block btn-flat">Sign out</a>
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{ url('/auth') }}">Login</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </header>
