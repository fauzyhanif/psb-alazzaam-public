    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="container">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2020 - created by <a href="https://wa.me/6282325494207">Fauzy Hanif</a>.</strong>
        </div>
        <!-- /.container -->
    </footer>
</div>
<!-- ./wrapper -->


<script>
    $(function () {
        $('.datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' });
        //Initialize Select2 Elements
        $('.select2').select2();
    });
</script>
</body>
</html>
