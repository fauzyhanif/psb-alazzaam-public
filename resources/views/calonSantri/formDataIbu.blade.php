<section>
    <h2 style="margin-bottom: 5px;">
        <b>E. Data Ibu</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group">
        <label>Nama Lengkap <span class="text-red">*</span></label>
        <input type="text" name="ibu_nama" value="{{ $dtIbu->nama }}" class="form-control required">
    </div>

    <div class="form-group">
        <label>Tempat Lahir</label>
        <input type="text" name="ibu_tmp_lahir" value="{{ $dtIbu->tmp_lahir }}" class="form-control">
    </div>

    <div class="form-group">
        <label>Tanggal Lahir (bulan/tanggal/tahun, contoh : 01/31/2004)</label>
        <input type="date" name="ibu_tgl_lahir" value="{{ $dtIbu->tgl_lahir }}" class="form-control">
    </div>

    <div class="form-group">
        <label>No HP / WA <span class="text-red">*</span></label>
        <input type="text" name="ibu_no_hp" value="{{ $dtIbu->no_hp }}" class="form-control required">
    </div>

    <div class="form-group">
        <label>Status</label>
        <div class="radio">
            <label>
                <input type="radio" name="ibu_status_hidup" class="required" value="Masih Hidup" {{ $dtIbu->status_hidup == 'Masih Hidup' ? 'checked' : '' }} checked> Masih Hidup
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="ibu_status_hidup" class="required" value="Wafat" {{ $dtIbu->status_hidup == 'Wafat' ? 'checked' : '' }}> Wafat
            </label>
        </div>
    </div>

    <div class="form-group">
        <label>NIK</label>
        <input type="text" name="ibu_nik" value="{{ $dtIbu->nik }}" class="form-control">
    </div>

    <div class="form-group">
        <label>Pendidikan Terakhir <span class="text-red">*</span></label>
        <select name="ibu_id_pendidikan" class="form-control required">
            <option value="">-- Pilih Pendidikan --</option>
            @foreach (HelperDataReferensi::DtPendidikan() as $data)
                <option value="{{ $data->id }}" {{ ($dtIbu->id_pendidikan == $data->id) ? 'selected' : '' }}>{{ $data->nama }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Pekerjaan <span class="text-red">*</span></label>
        <select name="ibu_id_pekerjaan" class="form-control required">
            <option value="">-- Pilih Pendidikan --</option>
            @foreach (HelperDataReferensi::DtPekerjaan() as $data)
                <option value="{{ $data->id }}" {{ ($dtIbu->id_pekerjaan == $data->id) ? 'selected' : '' }}>{{ $data->nama }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Domisili sesuai KK?</label>
        <div class="radio">
            <label>
                <input type="radio" name="ibu_domisili_sesuai_kk" class="required checked" value="Ya" {{
                    ($dtIbu->domisili_sesuai_kk == 'Ya') ? 'checked' : '' }}> Ya
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="ibu_domisili_sesuai_kk" class="required" value="Tidak" {{
                    ($dtIbu->domisili_sesuai_kk == 'Tidak') ? 'checked' : '' }}> Tidak
            </label>
        </div>
    </div>
</section>
