@extends('index')

@section('content')
<section class="content-header">
    <h1>
        Pendaftaran Berhasil
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> PSB</a></li>
        <li class="active">Data Calon Santri</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-success">
                <h4><i class="icon fa fa-check"></i> &nbsp; Berhasil!</h4>

                <p>
                    Selamat, pendaftaran atas Nama <b>{{ $data->nama }}</b> dengan No. Pendaftaran <b>{{ $data->no_pendaftaran }}</b> berhasil terdaftar di sistem kami.
                    <br>
                    <br>
                    Wajib dicatat, Anda mempunyai user login dengan username <b>{{ $data->no_pendaftaran }}</b> dengan password <b>{{ $data->password }}</b>
                    guna Upload Dokumen, Upload bukti pembayaran biaya pendaftaran dan lain-lain.
                    <br>
                    Selanjutnya silahkan <b>ikuti Tahapan-tahapan yang ada di box tahapan.</b>
                    <br>
                    Jika ada kebingungan atau pertanyaan silahkan hubungi Admin PSB <br>
                    Ust. Yusuf Imam Maulana, S.Pd. <br>
                    CP : 0858-0192-4860

                    <br>
                    <br>
                    Ustadzah Asma' Zahratus Shahihah<br>
                    CP : 0821-3638-5644

                    <br>
                    <br>
                    Ustadzah Feny Irminawati, S.T.<br>
                    CP : 0812-2963-3919
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Tahapan yang harus anda ikuti</h3>
                </div>
                <div class="box-body">
                    <div class="callout callout-info">
                        <h4><i class="icon fa fa-download"></i> &nbsp; Bukti Pendaftaran</h4>

                        <p><a href="{{ url('/download-berkas/bukti-pendaftaran',$data->no_pendaftaran) }}" target="_blank">Download sekarang</a></p>
                    </div>

                    <div class="callout callout-info">
                        <h4><i class="icon fa fa-download"></i> &nbsp; Informasi Biaya Pendaftaran</h4>

                        <p><a href="{{ url('/download-berkas/lanjutan-pendaftaran',$data->no_pendaftaran) }}" target="_blank">Download sekarang</a></p>
                    </div>

                    <div class="callout callout-info">
                        <h4><i class="icon fa fa-download"></i> &nbsp; Surat Kesanggupan</h4>

                        <p><a href="{{ url('/download-berkas/kesanggupan',$data->no_pendaftaran) }}" target="_blank">Download sekarang</a></p>
                    </div>

                    <div class="callout callout-info">
                        <h4><i class="icon fa fa-upload"></i> &nbsp; Upload Dokumen</h4>

                        <p><a href="{{ url('/calon-santri/upload-dokumen') }}">Upload sekarang</a></p>
                    </div>

                    <div class="callout callout-info">
                        <h4><i class="icon fa fa-upload"></i> &nbsp; Upload Bukti Transfer Biaya Pendaftaran</h4>

                        <p><a href="{{ url('calon-santri/upload-bukti-pembayaran') }}">Upload sekarang</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Data Pribadi
                    </h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-responsive test" style="margin-bottom: 50px">
                        <tbody>
                            <tr>
                                <td width="30%">No Pendaftaran</td>
                                <td width="70%">: <b>{{ $data->no_pendaftaran }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Nama</td>
                                <td width="70%">: <b>{{ $data->nama }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Username <span class="text-blue"><i>(Untuk login ke sistem)</i></span></td>
                                <td width="70%">: <b>{{ $data->no_pendaftaran }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Password <span class="text-blue"><i>(Untuk login ke sistem)</i></span></td>
                                <td width="70%">: <b>{{ $data->password }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">NISN</td>
                                <td width="70%">: <b>{{ $data->nisn }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">NIK</td>
                                <td width="70%">: <b>{{ $data->nik }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">No Kartu Kerluarga (KK)</td>
                                <td width="70%">: <b>{{ $data->kk }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Jenjang Tujuan</td>
                                <td width="70%">: <b>{{ $data->jenjang }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Jenis Kelamin</td>
                                <td width="70%">: <b>{{ $data->jns_kelamin == 'PA' ? 'PUTRA' : 'PUTRI' }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">TTL</td>
                            <td width="70%">: <b>{{ $data->tmp_lahir }}, {{ HelperDataReferensi::konversiTgl($data->tgl_lahir, 'T') }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Anak ke</td>
                                <td width="70%">: <b>{{ $data->anak_ke != NULL ? $data->anak_ke : '-' }} dari {{ $data->jml_sdr != NULL ? $data->jml_sdr : '-' }} bersaudara</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Golongan Darah</td>
                                <td width="70%">: <b>{{ $data->gol_darah }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Alamat</td>
                                <td width="70%">: <b>{{ $data->alamat }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">RT/RW</td>
                                <td width="70%">: <b>{{ $data->rt }}/{{ $data->rw }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Desa</td>
                                <td width="70%">: <b>{{ $data->id_desa }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kecamatan</td>
                                <td width="70%">: <b>{{ $data->kecamatan }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kota</td>
                                <td width="70%">: <b>{{ $data->kota }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Provinsi</td>
                                <td width="70%">: <b>{{ $data->provinsi }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Kode Pos</td>
                                <td width="70%">: <b>{{ $data->kode_pos != NULL ? $data->kode_pos : '-' }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Nomor Peserta PIP (Peserta Indonesia Pintar)</td>
                                <td width="70%">: <b>{{ $data->pip }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Status</td>
                                <td width="70%">: <b>{{ $data->status_anak }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Membaca Al Qur’an</td>
                                <td width="70%">: <b>{{ $data->membaca_alquran }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Penanggung Jawab Biaya</td>
                                <td width="70%">: <b>{{ $data->pj_biaya }}</b></td>
                            </tr>
                            <tr>
                                <td width="30%">Siapkah di uji hafalan qur’an 5 juz?</td>
                                <td width="70%">: <b>{{ $data->siap_diuji_hafalan }}</b></td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">Asal Sekolah</a></li>
                            <li><a href="#tab_2" data-toggle="tab">Data Ayah</a></li>
                            <li><a href="#tab_3" data-toggle="tab">Data Ibu</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table class="table table-striped table-responsive" style="margin-bottom: 50px">
                                    <tbody>
                                        <tr>
                                            <td width="30%">Nama Sekolah</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nama }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">NSPN</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nspn }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Kecamatan</td>
                                            <td width="70%">: <b>{{ $dtSekolah->kecamatan }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Kota</td>
                                            <td width="70%">: <b>{{ $dtSekolah->kota }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Provinsi</td>
                                            <td width="70%">: <b>{{ $dtSekolah->provinsi }}</b></td>
                                        </tr>

                                        @if ($data->id_jenjang == '1')
                                            <tr style="background: blue; color: white">
                                                <td colspan="2">Nilai Semester 1 Kelas 5 SD</td>
                                            </tr>
                                        @endif

                                        @if ($data->id_jenjang == '3' || $data->id_jenjang == '4')
                                            <tr style="background: blue; color: white">
                                                <td colspan="2">Nilai Semester 1 Kelas 8 SMP</td>
                                            </tr>
                                        @endif
                                        
                                        <tr>
                                            <td width="30%">Nilai Bhs Indonesia</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_indonesia }}</b></td>
                                        </tr>
                                        
                                        @if ($data->id_jenjang == '3' || $data->id_jenjang == '4')
                                            <tr>
                                                <td width="30%">Nilai Bhs Inggris</td>
                                                <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_inggris }}</b></td>
                                            </tr>
                                        @endif
                                        
                                        <tr>
                                            <td width="30%">Nilai Matematika</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nilai_matematika }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nilai Ipa</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nilai_ipa }}</b></td>
                                        </tr>

                                        @if ($data->id_jenjang == '1')
                                            <tr style="background: blue; color: white">
                                                <td colspan="2">Nilai Semester 2 Kelas 5 SD</td>
                                            </tr>
                                        @endif

                                        @if ($data->id_jenjang == '3' || $data->id_jenjang == '4')
                                            <tr style="background: blue; color: white">
                                                <td colspan="2">Nilai Semester 2 Kelas 8 SMP</td>
                                            </tr>
                                        @endif
                                        
                                        <tr>
                                            <td width="30%">Nilai Bhs Indonesia</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_indonesia_2 }}</b></td>
                                        </tr>
                                        
                                        @if ($data->id_jenjang == '3' || $data->id_jenjang == '4')
                                            <tr>
                                                <td width="30%">Nilai Bhs Inggris</td>
                                                <td width="70%">: <b>{{ $dtSekolah->nilai_bhs_inggris_2 }}</b></td>
                                            </tr>
                                        @endif
                                        
                                        <tr>
                                            <td width="30%">Nilai Matematika</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nilai_matematika_2 }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nilai Ipa</td>
                                            <td width="70%">: <b>{{ $dtSekolah->nilai_ipa_2 }}</b></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <table class="table table-striped table-responsive" style="margin-bottom: 50px">
                                    <tbody>
                                        <tr>
                                            <td width="30%">Nama</td>
                                            <td width="70%">: <b>{{ $dtAyah->nama }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nama</td>
                                            <td width="70%">: <b>{{ $dtAyah->no_hp }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Status Hidup</td>
                                            <td width="70%">: <b>{{ $dtAyah->status_hidup }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">NIK</td>
                                            <td width="70%">: <b>{{ $dtAyah->nik }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">TTL</td>
                                            <td width="70%">: <b>{{ $dtAyah->tmp_lahir . ', ' . HelperDataReferensi::konversiTgl($dtAyah->tmp_lahir, 'T') }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pendidikan terakhir</td>
                                            <td width="70%">: <b>{{ $dtAyah->pendidikan }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pendidikan terakhir</td>
                                            <td width="70%">: <b>{{ $dtAyah->pendidikan }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pekerjaan</td>
                                            <td width="70%">: <b>{{ $dtAyah->pekerjaan }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Alamat Sesuai KK?</td>
                                            <td width="70%">: <b>{{ $dtAyah->domisili_sesuai_kk }}</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <table class="table table-striped table-responsive" style="margin-bottom: 50px">
                                    <tbody>
                                        <tr>
                                            <td width="30%">Nama</td>
                                            <td width="70%">: <b>{{ $dtIbu->nama }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Nama</td>
                                            <td width="70%">: <b>{{ $dtIbu->no_hp }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Status Hidup</td>
                                            <td width="70%">: <b>{{ $dtIbu->status_hidup }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">NIK</td>
                                            <td width="70%">: <b>{{ $dtIbu->nik }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">TTL</td>
                                            <td width="70%">: <b>{{ $dtIbu->tmp_lahir . ', ' . HelperDataReferensi::konversiTgl($dtIbu->tmp_lahir, 'T') }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pendidikan terakhir</td>
                                            <td width="70%">: <b>{{ $dtIbu->pendidikan }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pendidikan terakhir</td>
                                            <td width="70%">: <b>{{ $dtIbu->pendidikan }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Pekerjaan</td>
                                            <td width="70%">: <b>{{ $dtIbu->pekerjaan }}</b></td>
                                        </tr>
                                        <tr>
                                            <td width="30%">Alamat Sesuai KK?</td>
                                            <td width="70%">: <b>{{ $dtIbu->domisili_sesuai_kk }}</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
