@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <h1>
            <a href="{{ url('/calon-santri/profil') }}" class="btn btn-default">
                <i class="fa fa-long-arrow-left"></i> Kembali
            </a>
        </h1>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> PSB</a></li>
        <li class="active">Data Calon Santri</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif

            @if ($data->file_bukti_pembayaran != '')
                @if ($data->konfirmasi_bukti_pembayaran == 0)
                    <div class="callout callout-info">
                        <h4>Sedang diverifikasi</h4>
                        <p>Mohohon ditunggu, admin sedang memverifikasi bukti pembayaran ananda.</p>
                    </div>
                @elseif($data->konfirmasi_bukti_pembayaran == 1)
                    <div class="callout callout-success">
                        <h4>Berhasil diverifikasi</h4>
                        <p>Alhamdulillah, bukti pembayaran ananda berhasil diferivikasi oleh admin.</p>
                    </div>
                @elseif($data->konfirmasi_bukti_pembayaran == 2)
                <div class="callout callout-danger">
                    <h4>Gagal diverifikasi</h4>
                    <p>Mohon maaf, bukti pembayaran ananda gagal diferivikasi oleh admin. Hubungi admin segera.</p>
                </div>
                @endif
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Upload Bukti Pembayaran
                    </h3>
                </div>
                <form action="{{ url('/calon-santri/do-upload-bukti-pembayaran') }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group" style="margin-top: 20px;">
                            <p>Tagihan anda sebesar</p>
                            <h3 style="margin: 0px"><b>@currency($data->biaya)</b> (Bayar Sesuai Nominal)</h3>
                        </div>

                        <div class="form-group" style="margin-top: 20px;">
                            <p>Silahkan transfer ke</p>
                            <h3 style="margin: 0px">
                                <b>
                                    Rek. Bank BSI <br>
                                    No rekening : 3333444417 <br>
                                    An PPTQ Al Azzaam
                                </b>
                            </h3>
                        </div>

                        <p class="text-blue">Setelah transfer, silahkan upload bukti pembayaran ananda di bawah ini.</p>

                        <div class="form-group">
                            <label>Upload File</label>
                            <input type="file" name="filename" class="form-control form-file">
                        </div>

                        <blockquote>
                            <p>
                                Format file berupa <span class="text-blue">.jpg .jpeg .png .pdf</span> <br>
                                Ukuran file maximal 1 mb
                            </p>
                        </blockquote>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-upload" style="display: none">
                            UPLOAD
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        History Upload Bukti Pembayaran
                    </h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <th>File</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="{{ url('/dokumen-pendaftar', $data->file_bukti_pembayaran) }}" target="_blank">{{ $data->file_bukti_pembayaran }}</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    // if file selected
    $('.form-file').change(function() {
        var filePath = this.value;

        // Allowing file type
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;

        if (!allowedExtensions.exec(filePath)) {
            alert('Invalid file type');
            this.value = '';
            return false;
        } else {
            var FileSize = this.files[0].size / 1024 / 1024; // in MB
            if (FileSize > 1) {
                alert("Mohon maaf file terlalu besar, maximal file 1 mb.")
                $('.btn-upload').css('display', 'none');
            } else {
                $('.btn-upload').css('display', 'block');
            }
        }
    });
</script>
@endsection
