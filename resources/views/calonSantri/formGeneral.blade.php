<section>
    <h2 style="margin-bottom: 5px;">
    <b>A. Data Pribadi</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group">
        <label>Nama Lengkap <span class="text-red">*</span></label>
        <input type="text" name="nama" class="form-control required" placeholder="Nama Lengkap" value="{{ $dtGeneral->nama }}">
    </div>

    <div class="form-group">
        <label>NISN <span class="text-red">*</span></label>
        <input type="text" name="nisn" class="form-control required" value="{{ $dtGeneral->nisn }}">
    </div>

    <div class="form-group">
        <label>NIK</label>
        <input type="text" name="nik" class="form-control" value="{{ $dtGeneral->nik }}">
    </div>

    <div class="form-group">
        <label>No Kartu Keluarga (KK)</label>
        <input type="text" name="kk" class="form-control" value="{{ $dtGeneral->kk }}">
    </div>

    <div class="form-group">
        <label>Jenis kelamin <span class="text-red">*</span></label>
        <div class="radio">
            <label>
                <input type="radio" name="jns_kelamin" class="required" value="PA" @if($dtGeneral->jns_kelamin == 'PA') checked @endif> Laki-laki
            </label>
            &nbsp;
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="jns_kelamin" class="required" value="PI" @if($dtGeneral->jns_kelamin == 'PI') checked @endif> Perempuan
            </label>
        </div>
    </div>

    <div class="form-group">
        <label>Tempat lahir <span class="text-red">*</span></label>
        <input type="text" name="tmp_lahir" class="form-control required" value="{{ $dtGeneral->tmp_lahir }}">
    </div>

    <div class="form-group">
        <label>Tanggal Lahir <span class="text-red">*</span> (bulan/tanggal/tahun, contoh : 01/31/2004)</label>
        <input type="date" name="tgl_lahir" class="form-control required" value="{{ $dtGeneral->tgl_lahir }}">
    </div>

    <div class="form-group">
        <label>Jumlah Saudara</label>
        <input type="text" name="jml_sdr" class="form-control" value="{{ $dtGeneral->jml_sdr }}">
    </div>

    <div class="form-group">
        <label>Anak ke</label>
        <input type="text" name="anak_ke" class="form-control" value="{{ $dtGeneral->anak_ke }}">
    </div>

    <div class="form-group">
        <label>Golongan Darah</label>
        <select name="gol_darah" class="form-control">
            <option value="-">-- Pilih Golongan Darah --</option>
            <option {{ $dtGeneral->gol_darah == 'A' ? 'selected' : '' }}>A</option>
            <option {{ $dtGeneral->gol_darah == 'B' ? 'selected' : '' }}>B</option>
            <option {{ $dtGeneral->gol_darah == 'AB' ? 'selected' : '' }}>AB</option>
            <option {{ $dtGeneral->gol_darah == 'O' ? 'selected' : '' }}>O</option>
        </select>
    </div>

    <div class="form-group">
        <label>Alamat <span class="text-red">*</span></label>
        <textarea name="alamat" id="" class="form-control required">{{ $dtGeneral->alamat }}</textarea>
    </div>

    <div class="form-group">
        <label>Rt <span class="text-red">*</span></label>
        <input type="number" name="rt" class="form-control required" value="{{ $dtGeneral->rt }}">
    </div>

    <div class="form-group">
        <label>Rw <span class="text-red">*</span></label>
        <input type="number" name="rw" class="form-control required" value="{{ $dtGeneral->rw }}">
    </div>

    <div class="form-group">
        <label>Provinsi <span class="text-red">*</span></label>
        <select name="id_provinsi"  class="form-control required" onchange="cariKota(this.value, 'GENERAL')">
            <option value="">-- Pilih Provinsi --</option>
            @foreach (HelperDataReferensi::DtProvinsi() as $data)
            <option value="{{ $data->id_wil }}" {{ $dtGeneral->id_provinsi == $data->id_wil ? 'selected' : '' }}>{{ $data->nm_wil }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Kota / Kabupaten <span class="text-red">*</span></label>
        <select name="id_kota"  class="form-control required" id="general-kota" onchange="cariKecamatan(this.value, 'GENERAL')">
            <option value="">-- Pilih Kota / Kabupaten --</option>
            @foreach (HelperDataReferensi::DtKotaByProvinsi($dtGeneral->id_provinsi) as $data)
                <option value="{{ $data->id_wil }}" {{ $dtGeneral->id_kota == $data->id_wil ? 'selected' : '' }}>{{ $data->nm_wil }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Kecamatan <span class="text-red">*</span></label>
        <select name="id_kecamatan"  class="form-control required" id="general-kecamatan">
            <option value="">-- Pilih Kecamatan --</option>
            @foreach (HelperDataReferensi::DtKecamatanByKota($dtGeneral->id_kota) as $data)
                <option value="{{ $data->id_wil }}" {{ $dtGeneral->id_kecamatan == $data->id_wil ? 'selected' : '' }}>{{ $data->nm_wil }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Desa <span class="text-red">*</span></label>
        <input type="text" name="id_desa" class="form-control required" value="{{ $dtGeneral->id_desa }}">
    </div>

    <div class="form-group">
        <label>Kode POS</label>
        <input type="text" name="kode_pos" class="form-control" value="{{ $dtGeneral->kode_pos }}">
    </div>

    <div class="form-group">
        <label>Status</label>
        <div class="radio">
            <label>
                <input type="radio" name="status_anak" class="required" value="Anak kandung" {{ ($dtGeneral->status_anak == 'Anak kandung') ? 'checked' : '' }}> Anak kandung
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="status_anak" class="required" value="Anak angkat" {{ ($dtGeneral->status_anak == 'Anak angkat') ? 'checked' : '' }}> Anak angkat
            </label>
        </div>
    </div>

    <div class="form-group">
        <label>Membaca Al Qur'an</label>
        <div class="radio">
            <label>
                <input type="radio" name="membaca_alquran" class="required" value="Sudah lancar" {{ ($dtGeneral->membaca_alquran == 'Sudah lancar') ? 'checked' : '' }}> Sudah lancar
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="membaca_alquran" class="required" value="belum lancar" {{ ($dtGeneral->membaca_alquran == 'belum lancar') ? 'checked' : '' }}> belum lancar
            </label>
        </div>
    </div>

    @if ($dtGeneral->id_jenjang == '1' || $dtGeneral->id_jenjang == '3')
        <div class="form-group">
            <label>Nomor Kartu Indonesia Pintar</label>
            <input type="text" name="pip" class="form-control" value="{{ $dtGeneral->pip }}">
        </div>
    @endif
</section>
