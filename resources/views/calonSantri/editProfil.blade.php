@extends('index')

@section('content')
@include('front.formulir.css')

<section class="content-header">
    <h1>
        Formulir Pendaftaran Santri Baru PPTQ Al Azzaam TA {{ HelperDataReferensi::ThnAkdAktif() }} Gelombang {{ HelperDataReferensi::GelombangAktif() }}
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="callout callout-info">
                        <h4>Perhatian</h4>

                        <p>1. Semua pertanyaan yang bertanda bintang (*) wajib diisi.</p>
                        <p>2. Dimohon untuk menyelesaikan edit data sampai selesai.</p>
                        <p>3. Jika ada kebingungan silahkan hubungi Admin PSB 0812-2963-3919 / 0856-4308-7035.</p>
                    </div>
                </div>
                <div class="box-body">
                    <form id="example-form" action="{{ url('/calon-santri/do-edit-profil', $dtGeneral->no_pendaftaran) }}" method="POST">

                        <input type="hidden" name="thn_akd" value="<?= substr(HelperDataReferensi::ThnAkdAktif(),0,4); ?>">
                        <input type="hidden" name="gelombang" value="<?= HelperDataReferensi::GelombangAktif(); ?>">

                        <div>
                            <h3>DATA PRIBADI </h3>
                            @include('calonSantri.formGeneral')

                            <h3>DATA SEKOLAH ASAL</h3>
                            @include('calonSantri.formSekolahAsal')

                            <h3>JENJANG TUJUAN</h3>
                            @include('calonSantri.formJenjangTujuan')

                            <h3>DATA AYAH</h3>
                            @include('calonSantri.formDataAyah')

                            <h3>DATA IBU</h3>
                            @include('calonSantri.formDataIbu')

                            <h3>DATA LAIN-LAIN</h3>
                            @include('calonSantri.formDataLain')

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('calonSantri.js')
@endsection
