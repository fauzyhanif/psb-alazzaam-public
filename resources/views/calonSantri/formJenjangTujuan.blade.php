<section>
    <h2 style="margin-bottom: 5px;">
        <b>C. Jenjang Tujuan</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group">
        <label>Jenjang Tujuan <span class="text-red">*</span></label>
        <div class="radio">
            @foreach (HelperDataReferensi::DtJenjang() as $data)
                <label>
                    <input type="radio" name="id_jenjang" class="required" value="{{ $data->id_jenjang }}" @if($dtGeneral->id_jenjang == $data->id_jenjang ) checked @endif> {{ $data->nama }}
                </label>
                &nbsp;
                &nbsp;
            @endforeach
        </div>
    </div>
</section>
