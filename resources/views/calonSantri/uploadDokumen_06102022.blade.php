@extends('index')

@section('content')
<section class="content-header">
    <h1>
        <h1>
            <a href="{{ url('/calon-santri/profil') }}" class="btn btn-default">
                <i class="fa fa-long-arrow-left"></i> Kembali
            </a>
        </h1>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-folder"></i> PSB</a></li>
        <li class="active">Data Calon Santri</li>
    </ol>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">
            @if ($status == 1)
                <div class="callout callout-sucess">
                    <h4>Dokumen ananda berhasil tervalidasi</h4>
                    <p>Alhamdulillah, semua dokumen ananda berhasil tervalidasi</p>
                </div>
            @elseif ($status == 2)
                <div class="callout callout-warning">
                    <h4>Mohon maaf, dokumen yang ananda upload ada yang harus direvisi</h4>
                    <p>Berikut catatan dari admin: {{ $catatan }}</p>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>
        <div class="col-md-7">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Form Upload Dokumen
                    </h3>
                </div>
                <div class="box-body">
                    <blockquote>
                        <p>
                            Format file berupa <span class="text-blue">.jpg .jpeg .png .pdf</span> <br>
                            Ukuran file maximal 1 mb <br>
                            Upload file satu persatu
                        </p>
                    </blockquote>

                    <table class="table table-striped">
                        <thead>
                            <th>Nama file</th>
                            <th>form file</th>
                            <th>Upload</th>
                        </thead>
                        <tbody>
                            @foreach ($dtForUpload as $item)

                            <tr>
                                <form action="{{ url('/calon-santri/do-upload-dokumen') }}" enctype="multipart/form-data" method="POST">
                                    @csrf

                                    <td>{{ $item->nm_dokumen }}</td>
                                    <td>
                                        <input type="file" name="filename" class="form-control form-file">
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <input type="hidden" name="id_dokumen" value="{{ $item->id_dokumen }}">
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-xs btn-upload" style="display: none">Upload</button>
                                    </td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Dokumen yang telah diupload
                    </h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Nama Dokumen</th>
                            <th>File</th>
                        </thead>
                        <tbody>
                            @foreach ($dtForUpload as $item)
                            <tr>
                                <td>{{ $item->nm_dokumen }}</td>
                                <td>
                                <a href="{{ url('dokumen-pendaftar', $item->file) }}" target="_blank">{{ $item->file }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    // if file selected
    $('.form-file').change(function() {
        var row = $(this).closest("tr");

        var filePath = this.value;

        // Allowing file type
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i;

        if (!allowedExtensions.exec(filePath)) {
            alert('Invalid file type');
            this.value = '';
            return false;
        } else {
            var FileSize = this.files[0].size / 1024 / 1024; // in MB
            if (FileSize > 1) {
                alert("Mohon maaf file terlalu besar, maximal file 1 mb.")
                row.find('.btn-upload').css('display', 'none');
            } else {
                row.find('.btn-upload').css('display', 'block');
            }
        }
    });
</script>
@endsection
