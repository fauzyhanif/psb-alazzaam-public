<section>
    <h2 style="margin-bottom: 5px;">
        <b>B. Sekolah Asal</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group">
        <label>Nama Sekolah <span class="text-red">*</span></label>
        <input type="text" name="sekolah_nama" value="{{ $dtSekolah->nama }}" class="form-control required">
    </div>

    <div class="form-group">
        <label>NSPN / NSM</label>
        <input type="text" name="sekolah_nspn" value="{{ $dtSekolah->nspn }}" class="form-control">
    </div>

    <div class="form-group">
        <label>Provinsi</label>
        <select name="sekolah_id_provinsi"  class="form-control" onchange="cariKota(this.value, 'SEKOLAH')">
            <option value="">-- Pilih Provinsi --</option>
            @foreach (HelperDataReferensi::DtProvinsi() as $data)
            <option value="{{ $data->id_wil }}" {{ $dtSekolah->id_provinsi == $data->id_wil ? 'selected' : '' }}>{{ $data->nm_wil }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Kota / Kabupaten</label>
        <select name="sekolah_id_kota"  class="form-control" id="sekolah-kota" onchange="cariKecamatan(this.value, 'SEKOLAH')">
            <option value="">-- Pilih Kota / Kabupaten --</option>
            @foreach (HelperDataReferensi::DtKotaByProvinsi($dtSekolah->id_provinsi) as $data)
                <option value="{{ $data->id_wil }}" {{ $dtSekolah->id_kota == $data->id_wil ? 'selected' : '' }}>{{ $data->nm_wil }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Kecamatan</label>
        <select name="sekolah_id_kecamatan"  class="form-control" id="sekolah-kecamatan">
            <option value="">-- Pilih Kecamatan --</option>
            @foreach (HelperDataReferensi::DtKecamatanByKota($dtSekolah->id_kota) as $data)
                <option value="{{ $data->id_wil }}" {{ $dtSekolah->id_kecamatan == $data->id_wil ? 'selected' : '' }}>{{ $data->nm_wil }}</option>
            @endforeach
        </select>
    </div>

    @if ($dtGeneral->id_jenjang == '1')
        <div class="info-title" style="background: blue; color: white; padding: 4px 6px"><b>Nilai Semester 1 Kelas 5 SD</b></div>
    @endif

    @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
        <div class="info-title" style="background: blue; color: white; padding: 4px 6px"><b>Nilai Semester 1 Kelas 8 SMP</b></div>
    @endif
    

    @if ($dtGeneral->id_jenjang == '1' || $dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
        <div class="form-group">
            <label>Nilai Bhs Indonesia (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_bhs_indonesia" value="{{ $dtSekolah->nilai_bhs_indonesia }}" class="form-control required">
        </div>        

        <div class="form-group">
            <label>Nilai Matematika (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_matematika" value="{{ $dtSekolah->nilai_matematika }}" class="form-control required">
        </div>

        <div class="form-group">
            <label>Nilai IPA (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_ipa" value="{{ $dtSekolah->nilai_ipa }}" class="form-control required">
        </div>
    @endif

    @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
        <div class="form-group">
            <label>Nilai Bhs Inggris (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_bhs_inggris" value="{{ $dtSekolah->nilai_bhs_inggris }}" class="form-control required">
        </div>
    @endif

    @if ($dtGeneral->id_jenjang == '1')
        <div class="info-title" style="background: blue; color: white; padding: 4px 6px"><b>Nilai Semester 2 Kelas 5 SD</b></div>
    @endif

    @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
        <div class="info-title" style="background: blue; color: white; padding: 4px 6px"><b>Nilai Semester 2 Kelas 8 SMP</b></div>
    @endif
    

    @if ($dtGeneral->id_jenjang == '1' || $dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
        <div class="form-group">
            <label>Nilai Bhs Indonesia (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_bhs_indonesia_2" value="{{ $dtSekolah->nilai_bhs_indonesia_2 }}" class="form-control required">
        </div>        

        <div class="form-group">
            <label>Nilai Matematika (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_matematika_2" value="{{ $dtSekolah->nilai_matematika_2 }}" class="form-control required">
        </div>

        <div class="form-group">
            <label>Nilai IPA (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_ipa_2" value="{{ $dtSekolah->nilai_ipa_2 }}" class="form-control required">
        </div>
    @endif

    @if ($dtGeneral->id_jenjang == '3' || $dtGeneral->id_jenjang == '4')
        <div class="form-group">
            <label>Nilai Bhs Inggris (contoh: 95.50) <span class="text-red">*</span></label>
            <input type="text" name="nilai_bhs_inggris_2" value="{{ $dtSekolah->nilai_bhs_inggris_2 }}" class="form-control required">
        </div>
    @endif


</section>
