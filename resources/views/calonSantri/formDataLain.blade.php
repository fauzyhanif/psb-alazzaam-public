<section>
    <h2 style="margin-bottom: 5px;">
        <b>F. Data Lain-lain</b>
    </h2>
    <hr style="border-top: 1px solid black;">

    <div class="form-group">
        <label>Penanggung Jawab Biaya <span class="text-red">*</span></label>
        <div class="radio">
            <label>
                <input type="radio" name="pj_biaya" class="required" value="Orang Tua" {{ $dtGeneral->pj_biaya == 'Orang Tua' ? 'checked' : '' }}> Orang Tua
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="pj_biaya" class="required" value="Wali" {{ $dtGeneral->pj_biaya == 'Wali' ? 'checked' : '' }}> Wali
            </label>
        </div>
    </div>

    <div class="form-group">
        <label>Siapkah di uji hafalan qur’an 5 juz? <span class="text-red">*</span></label>
        <div class="radio">
            <label>
                <input type="radio" name="siap_diuji_hafalan" class="required" value="Ya" {{ ($dtGeneral->siap_diuji_hafalan == 'Ya') ? 'checked' : '' }}> Ya
            </label>
            &nbsp;
            &nbsp;
            <label>
                <input type="radio" name="siap_diuji_hafalan" class="required" value="Tidak" {{ ($dtGeneral->siap_diuji_hafalan == 'Tidak') ? 'checked' : '' }}> Tidak
            </label>
        </div>
    </div>
</section>
